+++
title="Cycle de vie d'une identité dans la toile de confiance Ğ1"
# date = 2024-10-17
weight = 20
sort_by = "weight"

# [taxonomies]
# authors = ["Matograine",]
+++



# Cycle de vie d'une identité dans la toile de confiance Ğ1

Le fonctionnement monétaire de Ğ1 implique que chaque individu soit possède une identité unique, à laquelle est lié un seul compte. Ğ1 arrive à ce résultat grâce à une *toile de confiance*, ou *toile relationnelle*. 

[Fonctionnement de la toile de confiance](@/wiki/toile-de-confiance/_index.md)

Cette page vous présente les opérations qui ont lieu durant la vie d'une identité sur l'infrastructure V2 de la monnaie Ğ1.

[TOC]

## Adhésion

Pour illustrer le processus d'adhésion, on prendra l'exemple d'Alice, qui souhaite intégrer la toile de confiance. Alice possède déjà un compte Ğ1.

### Avant l'adhésion

Avant l'adhésion, Alice doit avoir rencontré au moins <span class="blockchain_param">5</span> personnes déjà membres qui sont d'accord pour la certifier. Ces personnes sont tenues de respecter la [licence Ğ1](@/wiki/g1/licence-g1.md), donc entre autres, de bien connaître Alice.

Par ailleurs, son compte doit être provisionné d'au moins 2 Ğ1. Ce montant sert à payer des frais qui seront remboursés si tout se passe bien. Il couvre :
- 1 Ğ1 de dépôt existentiel (en-dessous, le compte n'est pas enregistré)
- 1 Ğ1 pour pouvoir avancer des frais

Par la suite, 10 Ğ1 supplémentaires seront nécessaires, mais le processus peut commencer avec seulement 2 Ğ1.

### Invitation

Alice va demander à son amie Barbara de l'inviter, en lui indiquant l'adresse de son compte. Barbara lui envoie donc une invitation, qui est également sa première certification. A partir de ce moment, Alice a <span class="blockchain_param">**48h**</span> pour confirmer son invitation (ce délai est celui mis en place sur le réseau de développement, il sera sans doute plus long en production)

### Confirmation de l'invitation et création de l'identité

Alice va confirmer la réception de son invitation en indiquant un *pseudonyme d'identité*. En effet, les identités sont simplement numérotées. Avoir un pseudonyme permet de retrouver l'identité d'Alice plus facilement.

A ce moment, Alice **doit** enregistrer un document de révocation, qui lui permettra de révoquer son identité si, par exemple, elle perd l'accès à son compte ou se le fait pirater.

Alice enregistre donc ce document de révocation, et **elle se l'envoie à elle-même par email**. Il est essentiel de stocker son document de révocation sur un autre appareil que celui sur lequel son compte est enregistré, car si l'appareil est perdu ou volé, on a toujours accès à ce document.

### Réception des autres certifications

Après qu'Alice a confirmé l'invitation, les quatres autres personnes (ou plus) peuvent envoyer leurs certifications à Alice. Ces autres personnes sont tenues de vérifier que Alice a sauvegardé son document de révocation. 

Alice a un délai de <span class="blockchain_param">**deux mois**</span> pour recevoir ces autres certifications. Si Alice ne passe pas membre, les certifications reçues sont invalidées et reviennent dans le stock de certifications disponibles pour leurs émetteurs.

### Demande d'adhésion

Une fois qu'Alice a reçu au moins cinq adhésions, elle peut faire une demande d'adhésion. Cette demande va provoquer une évaluation de la règle de distance : Alice ne doit pas être trop éloignée, en termes relationnels, du reste de la toile de confiance. Pour faire cette demande, Alice doit avoir une caution de 10 Ğ1 sur son compte, qui sera bloquée, et éventuellement prélevée si sa demande n'est pas validée. La demande d'adhésion peut aussi être effectuée par un des membres certificateurs, dans ce cas c'est le certificateur qui doit bloquer la caution de 10 Ğ1.

Au moment de faire cette demande, le logiciel utilisé par Alice (ou son certificateur) lui indique qu'elle ne respecte pas la règle de distance, et qu'elle ne devrait donc pas faire cette demande pour éviter de payer la caution inutilement.

Alice va donc demander à une sixième personne de la certifier : sa soeur, qui habite à l'autre bout de la France et est en lien avec une autre 'région' de la toile de confiance.

Suite à cette sixième certification, une nouvelle demande d'adhésion est faite. L'identité d'Alice devient membre. Elle peut désormais certifier d'autres membres,à raison d'une certification tous les <span class="blockchain_param">5</span> jours, et co-créer le dividende universel.

## Utilisation habituelle

Une fois que Alice est membre, son identité pourra faire différentes actions :

### Co-création du Dividende Universel

Sur la monnaie Ğ1, le dividende universel (D.U.) est co-créé chaque jour, aux alentours de midi. Le fait qu'un compte soit membre au moment de la création du D.U. lui *donne le droit* de réclamer ce D.U. mais ne le crée pas immédiatement. Ainsi, si quelqu'un n'a pas utilisé son compte durant une semaine, tous les D.U. de la semaine seront créés lorsqu'il les réclamera, ce qui entraîne une charge mieux répartie dans le temps pour le réseau.

Ceci devrait être transparent pour Alice, car les logiciels clients devraient :
- Faire la réclamation des D.U. automatiquement lors d'une autre action, par exemple lors d'une transaction. Il n'y aura pas de bouton 'réclamer le DU' sauf pour des usages avancés.
- Faire la somme du montant du compte et des D.U. restants à réclamer pour afficher le montant correct du compte.

### Certifications

- Quand Alice voudra certifier une personne la première fois, il lui faudra l'*inviter*. Et dans ce cas il faudra lui demander l'adresse complète de son compte, car son identité n'aura pas encore été définie.
- Alice ne pourra émettre une certification que tous les <span class="blockchain_param">5</span> jours, et ces certifications seront enregistrées immédiatement.

### Adhésion

<!-- [source sur le forum](https://forum.duniter.org/t/trancher-sur-la-perte-du-statut-de-membre/10936/7)
[source 2](https://forum.duniter.org/t/proposition-de-supprimer-la-notion-didentite-desactivee-mais-non-revoquee-et-la-notion-dadhesion/9051/19) -->

Le Dividende universel est supposé être créé sur un seul compte par individu vivant. Le système doit maintenir le statut de membre pour les identités 1. dont le compte lié est actif, et 2. qui respectent toujours la règle de distance. Pour cela, Alice devra demander au moins <span class="blockchain_param">une fois par an</span> une réévaluation de leur respect de la règle de distance. Ceci apparaîtra dans les clients sous la forme d'un bouton "renouveler l'adhésion". Deux cas peuvent se présenter :

- Si le logiciel client prévoit que le résultat sera positif, la demande est envoyée, le résultat est positif, l'identité reste membre.
- Si le logiciel client prévoit que le résultat sera négatif, il bloquera l'envoi (ou affichera un avertissement) pour éviter qu'Alice paie une [caution de 10Ğ1](content/wiki/doc-v2/concept/frais.md#gestion-des-frais-dans-le-cas-d-actions-liees-a-la-toile-de-confiance). Alice doit alors trouver de nouvelles certifications qui lui permettront de respecter la règle de distance.
  - si elle les trouve, elle peut redemander l'évaluation et son adhésion est renouvelée
  - si elle ne les trouve pas, l'identité sera désactivée un an après la dernière évaluation positive.

## Fin de vie du compte

Si Alice meurt, ou si elle ne souhaite plus faire partie de la toile de confiance (elle s'en désintéresse), le système doit le détecter et l'exclure de la toile de confiance. La révocation d'une identité membre est automatique dans ce cas. Elle peut aussi être manuelle si Alice décide de révoquer son identité membre, par exemple suite à un vol de sa phrase de passe. 

Après une révocation, les certifications qui ont été *émises* et *reçues* par l'identité révoquée restent valides pour leur durée normale (deux ans après leur émission). Cependant, si la propriétaire du compte souhaite réintégrer la toile de confiance, il lui faudra reprendre le processus d'adhésion depuis le début car c'est une *nouvelle identité* qui devra être créée. 

Voici différents scénarios pour la fin de vie d'un compte :

### Révocation manuelle

Alice publie son document de révocation. La révocation est prise en compte dès le bloc suivant.

### Désactivation puis révocation automatique

L'identité d'Alice passe désactivée si elle n'a pas fait l'objet d'une évaluation positive de la règle de distance depuis <span class="blockchain_param">un an</span>. Elle n'a plus le droit de créer le Dividende Universel, mais conserve ses certifications. A partir de ce moment, Alice a <span class="blockchain_param">un mois</span> pour trouver les certifications manquantes et redemander une évaluation de la règle de distance, après quoi son identité sera révoquée automatiquement.
