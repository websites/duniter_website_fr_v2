+++
title="Sous-toile forgerons"
# date = 2024-10-17
weight = 20
sort_by = "weight"

# [taxonomies]
# authors = ["Matograine",]
+++



# Sous-toile forgerons

Seuls les membres de la sous-toile forgerons peuvent forger des blocs.
Tout le monde peut toujours faire tourner un nœud miroir qui n’écrit pas les blocs mais répond aux demandes des clients. Les nœuds forgerons se consacrant au calcul et à l’écriture des blocs, ils ne répondent pas aux demandes des clients.
Tous les nœuds communiquent entre eux de façon quasi instantanée.

## Certifications

Les certifications forgerons doivent respecter une [licence forgeron (actuellement en brouillon)](https://git.duniter.org/1000i100/g1_charter/-/blob/master/smith_charter/g1_smith_charter_fr.adoc?ref_type=heads) qui assure un bon niveau de sécurité, entre autres avoir déjà fait tourner correctement un nœud miroir depuis un certain temps, pouvoir garder son serveur ouvert 24H/24 et 7j/7 et avoir une bonne connexion internet.
Les membres de cette sous-toile n’ont pas nécessairement besoin de se connaître ou de se voir physiquement, car ils doivent d’abord être membres de la Toile de Confiance des Cocréateurs. Les certifications de la sous-toile forgerons indiquent que les certifiants jugent que les capacités techniques des certifiés sont suffisantes.

Pour faire partie de cette sous-toile forgerons dès le démarrage, il faut faire tourner un nœud avant la bascule sur la gdev ou gtest (les monnaies qui servent à tester la version 2 avant le démarrage).

## Maintien d'un noeud forgeron

L'appartenance à la toile de confiance forgeron n'est utile à l'utilisatrice que si elle souhaite maintenir un noeud forgeron. Voici la documentation pour en [installer un](@/wiki/doc-v2/installer/_index.md).
