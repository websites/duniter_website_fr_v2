+++
title="Frais et quotas"
# date = 2024-10-17
weight = 20
sort_by = "weight"

# [taxonomies]
# authors = ["Matograine",]
+++



# Frais et quotas

## Une blockchain n’est jamais illimitée.

Une blockchain a besoin d’une puissance de calcul et d’un espace de stockage. Les deux, bien que conséquents, ne sont pas illimités. Une attaque possible est la saturation de la puissance de calcul par envoi de milliards de transactions à la seconde.
Les autres blockchains prélèvent des frais pour chaque action afin de dissuader cette saturation des calculs et de l’espace de stockage.
Mais la Ğ1 n’est pas comme les autres. Des frais sont bien prévus, mais la blockchain les rembourse la plupart du temps. Les frais doivent dissuader un attaquant, mais ne devraient pas être sensibles la plus grande partie du temps.

## Paiement et remboursement des frais de transaction

### Les frais ne seront prélevés qu’en cas de surcharge de la blockchain.

- Un nombre total d’actions (transactions, certifications, adhésion, …) par bloc a été evalué comme étant la limite d’un fonctionnement "normal". Au-delà la de cette limite la blockchain est considérée en saturation.
- C’est uniquement si le nombre d’actions dans un bloc dépasse cette limite que des frais sont prélevés. Frais estimés à environ 0.015 DUĞ1, soit 17 Ğ1 pour 100 transactions.

### Les frais seront remboursés à tous les membres de la toile de confiance

- Un quota d’actions par membre et par bloc est défini. Ce quota correspond à <span class="blockchain_param" code_name="MaxQuota">10</span>Ğ1 de frais offerts par bloc, ce qui correspond à environ 50 transactions effectuées toutes les 6 secondes.
- Les comptes non membres pourront être liés à un compte membre et se faire rembourser aussi leur frais, dans la limite du quota par membre.
- Un membre peut faire une seule transaction mais si, au même moment, quelqu’un lance 1 million de transactions sur 1 million de comptes, la blockchain sera saturée, ce membre sera donc prélevé de frais. Puis il sera remboursé parce qu’il ne dépasse pas le quota par membre.
- Si ce sont un membre et ses comptes liés qui lancent des centaines de transactions entraînant la saturation, ce membre ne sera remboursé que sur ses premières transactions, les suivantes étant au-delà du quota.
- Si un membre lance des centaines de transactions même au-delà du quota par membre mais qu’il est tout seul à faire des transactions à ce moment là, cela peut passer sans saturer la blockchain, donc pas de frais.
- Par contre, pas de remboursement possible en cas de fermeture de compte !

Il est peu probable qu’une telle attaque soit déclenchée car elle entraînerait la ruine de l’attaquant, pour un blocage temporaire. Ces frais sont donc une dissuasion.

## Gestion des frais dans le cas d'actions liées à la toile de confiance

D'autres actions que les transactions entraînent des frais :
- certification
- évaluation de la règle de distance
- révocation

Pour la plupart de ces actions, le remboursement des frais se fait de façon similaire :

- si vous invitez un membre, les frais vous seront remboursés même s'il ne confirme pas l'invitation.
- si vous certifiez une identité et qu'elle ne devient pas membre, vos frais sont tout de même remboursés.
- Lorsque vous révoquez une identité, les frais sont toujours remboursés.

En revanche, pour une évaluation de la règle de distance, qui est un calcul coûteux, il y a une *caution* qui peut s'appliquer. Le calcul de règle de distance étant coûteux, il s'agit de ne traiter que des demandes qui doivent aboutir.
Quand vous faites une demande d'adhésion pour une identité, <span class="blockchain_param" code_name="EvaluationPrice">10</span>Ğ1 sont bloqués temporairement sur votre compte.
Si l'évaluation est négative, ce montant vous sera retiré.
Pour éviter que vous risquiez de perdre de la monnaie, les logiciels clients auront une prévision du résultat et vous avertiront (ou vous bloqueront) avant de faire la demande.
