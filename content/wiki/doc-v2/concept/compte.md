+++
title="Le compte sur Duniter V2"
# date = 2024-10-17
weight = 20
sort_by = "weight"

# [taxonomies]
# authors = ["Matograine",]
+++



# Définition d'un compte sur Duniter V2

Cette page vise à donner des précisions sur ce qu'est un compte sur Duniter V2, et comment ils sont créés et utilisés.

[TOC]

## Trousseau de clefs

Un compte est un [trousseau de clefs cryptographique](https://fr.wikipedia.org/wiki/Cryptographie_asym%C3%A9trique), composé d'une *clef privée* (un grand nombre -sur 32 octets- qui permet de signer des documents) et d'une *clef publique* associée, qui permet de *chiffrer* les documents et désigner le trousseau comme destinataire. Les deux clefs sont liées mathématiquement de sorte que :

- si on connaît une clef publique, on peut **vérifier** si un document a été signé par la clef privée associée (donc par la propriétaire du compte)
- si on connaît une clef privée (donc si on est propriétaire du compte), on peut **déchiffrer** les documents qui ont été chiffrés en utilisant la clef publique. On peut également signer un document qui sera vérifiable avec la clef publique.

Dans le cas de Ğ1, la clef publique permet d'indiquer 'je transfère de la monnaie vers le compte A et je veux que seule la propriétaire du compte A puisse la dépenser à nouveau'. La clef privée permet de signer la transaction.

La *clef privée* est destinée à être connue uniquement par la propriétaire du compte, et doit être tenue secrète. Si la clef privée est révélée à  un tiers, ceci permet au tiers de faire des actions au nom de la propriétaire, sans qu'il soit possible de distinguer qui a fait quoi.

### Précisions sur la notion de compte "membre"

Un compte n'est pas *intrinsèquement* "membre" ou "portefeuille". Sur Ğ1, un compte peut être lié à une *identité*. C'est le lien avec cette identité qui détermine si un compte est "membre" ou pas. C'est l'identité, et non le compte, qui reçoit les certifications, même si par abus de language on parle de "certifier un compte membre".

## Adresse

Le framework Substrate utilise le format SS58 pour générer les adresses. Ce format contient :
- un préfixe qui indique le réseau sur lequel l'adresse est valide (`4450` pour le réseau Ğ1, `42` pour le réseau ĞDev)
- la clef publique
- une somme de vérification permettant de vérifier la cohérence des informations précédentes.

Ces trois informations sont groupées au format binaire, puis converties dans le format base58 pour être lisible par des humains. Ainsi, une même clef publique donnera deux adresses totalement différentes pour deux réseaux :
```bash
# adresse sur réseau GDev (préfixe=42)
5GrwvaEF5zXb26Fz9rcQpDWS57CtERHpNehXCPcNoHGKutQY
# adresse sur réseau Ğ1 (préfixe=4450, ...)
g1Pn1geRZnHcVdWdKkoYwQrAARDTXK1D7H3zuXj7GNayT1thu
```

Ce format a deux avantages majeurs :

- Il contient un préfixe de réseau, donc on ne peut pas faire une transaction sur le réseau Ğ1 en pensant être sur le réseau de test
- Il contient une somme de vérification, qui empêche de faire des virements si on a fait une erreur en recopiant l'adresse.

Le préfixe `4450` a été choisi car toutes les adresses du réseau Ğ1 commenceront par `g1`.

## Mnémonique

Un mnémonique est une phrase de 12 mots choisis aléatoirement parmi une liste de 2048 mots prédéfinis. C'est le secret qui va permettre de générer un trousseau de clefs, c'est-à-dire un compte. Ces 12 mots représentent un nombre tiré au hasard. Si deux personnes tirent les mêmes 12 mots dans le même ordre, alors elles ont accès au même compte. Mais ce risque est si imporbable qu'il est considéré nul en pratique.

Voici un exemple de mnémonique en anglais : 
```
deposit kite bleak skate early flag fabric picture exile consider express size
```

Ce mnémonique correspond au compte `5FRx9wBiNAVragvnCXQdegpYQCN74WJxANnx6hFF1McW3Ei3` sur le réseau de ĞDev.

## Dérivation

Les clefs publiques peuvent être *dérivées*, c'est-à-dire qu'on peut générer des comptes "enfants" d'un autre compte.
Le compte généré à partir du mnémonique est appelé la *racine* : c'est à partir de ce compte qu'on va dériver tous les autres. C'est cette propriété qui permet d'avoir un "coffre-fort" avec différents comptes, en utilisant un seulmnémonique. Le schéma de dérivation suit le standard bip32.

La dérivation est notée, par exemple `//2` pour la dérivation d'un compte selon le chiffre 2. Prenons un exemple, avec le mnémonique qu'on a donné en exemple plus haut :

- `5FRx9wBiNAVragvnCXQdegpYQCN74WJxANnx6hFF1McW3Ei3` est l'adresse du compte racine sur le réseau ĞDev.
- `5DSQLMy6LBinuMQSZGa83NkurD1eH2tcLxi72e5mhK5snQq3` est l'adresse de la dérivation `//2`

Si vous voulez faire des essais, l'extension [duniter-connect](https://addons.mozilla.org/fr/firefox/addon/duniter-connect/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search) le permet.

### Dérivation des comptes membres

Par convention, les comptes membres sont créés sur le compte racine (donc sans dérivation).

### Import de comptes dérivés

Si vous devez importer le mnémonique d'un compte dérivé et que le logiciel client ne donne pas l'adresse attendue, vous pouvez entrer le mnémonique et la dérivation ( par exemple `//2` à la fin):

```
deposit kite bleak [...] consider express size//2
```

### Usages avancés de la dérivation

La dérivation hierarchique déterministe des clef ("*hierarchically deterministic wallet*") permet de nombreuses possibilités. En voici trois :

#### Dérivation par mot

Une dérivation peut également être faite par un mot, par exemple `//gdev`

#### Dérivation sur plusieurs niveaux

Il est possible de définir un *chemin de dérivation*, par exemple `//gdev//3`

#### Oneshot account

Pour certains aspects techniques ou pour des aspects de confidentialité, il est possible de dériver des comptes "à usage unique" (`oneshot account`), qui recevront un unique versement et pourront le dépenser en même temps que d'autres `oneshot account` s'il y a besoin d'une somme qu'ils ne contiennent pas. Ces comptes sont destinés à n'être utilisés qu'une seule fois.
