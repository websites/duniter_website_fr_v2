+++
title="Concepts de la Ğ1 V2"
# date = 2024-10-17
weight = 10
sort_by = "weight"

# [taxonomies]
# authors = ["Matograine",]
+++



# Principaux concepts de l'infrastructure V2

Duniter V2 est basé sur le *framework* Substrate. Plusieurs logiciels satellites sont nécessaires à un usage facile de la blockchain. Cette partie page vise à présenter les concepts-clefs de cette infrastructure :

## Concepts de base

- [👛 Comptes](@/wiki/doc-v2/concept/compte.md)
- [💸 Frais et quotas](@/wiki/doc-v2/concept/frais.md)
- [🤝 Toile de confiance](@/wiki/doc-v2/concept/tdc.md)
- [⚒️ Toile de confiance forgerons](@/wiki/doc-v2/concept/forgerons.md)

## Clients

Les logiciels clients servent d'interface pour accéder aux données blockchain. Certains vous servent à consulter l'état de votre compte et à faire des actions. D'autres donnent une vue globale de l'état de la blockchain. Voici une liste non exhaustive :

### Gérer son compte et faire des transactions

Les liens pointent vers les dépôts Git des logiciels, qui n'ont pas encore de site dédié.

- [Ğecko](https://git.duniter.org/clients/gecko) : application smartphone pour gérer votre compte.
- [Cesium²](https://git.duniter.org/clients/cesium-grp/cesium2s) : application smartphone, bureau, extension de navigateur pour gérer votre compte.
- [Tikka](https://git.duniter.org/clients/python/tikka) : application de bureau pour gérer votre compte, qui vise des usages comptables.
- [Duniter-connect](https://git.duniter.org/clients/duniter-connect) : extension de navigateur, qui permet de signer des transactions. Elle ne sert à rien toute seule, mais permet de faire des opérations de façon sécurisée par l'intermédiaire de sites web.
- [Ğcli](https://git.duniter.org/clients/rust/gcli-v2s/) : un client en ligne de commande.

<!-- ### Observer la blockchain

TODO - y mettre un futur WotWizard -->

### Usages avancés

- [PoljadotJS](https://polkadot.js.org/apps/#/explorer) : un client web, générique à l'écosystème Substrate. Permet des opérations avancées. N'est pas conçu pour un usage courant.
- [Duniter-portal](https://git.duniter.org/clients/duniter-portal) : fork de PolkadotJS spécifique à Duniter V2.
- [Télémétrie Polkadot](https://telemetry.polkadot.io/#list/0xc184c4ccde8e771483bba7a01533d007a3e19a66d3537c7fd59c5d9e3550b6c3) : permet d'avoir des informations sur un certain nombre de noeuds.

## Infrastructure

La nouvelle infrastructure prévoit quatre types de serveurs pour gérer les données Blockchain (transactions, certifications, etc.) et les données annexes (historique des transactions, annonces, etc.). 

### Noeud miroir

Les noeuds miroir sont directement accessibles par les logiciels clients, sur une adresse de type `wss://g1.coinduf.eu`. Ils ont pour rôle de **mettre à disposition les données blockchain aux clients**. Par défaut, ces noeuds mettent à disposition des informations sur *l'état actuel* de la blockchain, mais ils ne conservent que l'historique récent des blocs. Certains noeuds miroir ont un rôle d'*archive* et conservent l'ensemble des données blockchain depuis le démarrage.

Les noeuds miroir sont installés avec le logiciel [Duniter V2](https://git.duniter.org/nodes/rust/duniter-v2s). Un [tutoriel](@/wiki/doc-v2/installer/miroir/_index.md) vous indique comment en mettre un en place.

### Noeud forgeron

Les noeuds forgeron ne sont pas directement accessibles par les logiciels clients. Leur rôle est d'**inscrire les données dans la blockchain**. Ils communiquent avec les autres noeuds (miroir et forgerons) par un protocole pair-à-pair qui n'est pas destiné aux logiciels clients. Ils ne conservent que l'historique récent de la blockchain à des fins de vérification.

Les noeuds forgerons sont installés avec le logiciel [Duniter V2](https://git.duniter.org/nodes/rust/duniter-v2s).

#### Oracle de distance

Certains noeuds forgerons ont un rôle d'*oracle*. À intervalles réguliers, ils calculent le respect de la règle de distance pour les identités qui en ont fait la demande, et publient ce résultat en blockchain.

Les oracles utilisent le même logiciel que les noeuds Duniter, avec une [configuration spécifique](https://duniter.org/wiki/duniter-v2/distance-oracle/).

### Datapod

Les Datapods servent à stocker des données annexes à la Blockchain. Par exemple, ils peuvent stocker l'avatar d'un compte, des petites annonces, ou des messages. Ils sont synchronisés en pair-à-pair par le protocole *IPFS*.

Ils sont installés avec le logiciel [ipfs-datapod](https://git.duniter.org/nodes/ipfs-datapod/).

### Indexeur

Les indexeurs servent à rendre plus facile le traitement des données blockchain par les clients : ils enregistrent ces données pour conserver un historique des évènements. Par ailleurs, ils les ordonnent pour faciliter la récupération de données liées (par exemple, récupérer le pseudonyme de l'identité d'un compte en même temps que son historique de transactions). Les indexeurs s'appuient sur un noeud miroir archive pour 'écouter' la blockchain.

Ils sont installés avec le logiciel [Duniter-squid](https://git.duniter.org/nodes/duniter-squid)

## Gouvernance 

Les évolutions des règles Blockchain sont votées par un *comité technique*. Ces évolutions s'appliquent à tous les forgerons. Ceci évite de générer des *forks* involontaires.

Au démarrage de la nouvelle infrastructure, ce comité technique est composé des développeurs du coeur. L'intention est de poser des règles formelles pour étendre cette gouvernance à d'autres utilisateurs. Mais ceci devra faire l'objet d'un chantier spécifique.

### Trésorerie

Les frais prélevés sur les transactions ne sont pas détruits, ils vont sur un compte spécifique de trésorerie commune. Les règles de dépense de cette trésorerie n'ont pas encore été fixées. Au lancement de la nouvelle infrastructure, il n'est pas prévu que quiconque puisse débloquer ces fonds. Ceci devra faire l'objet d'un chantier spécifique.