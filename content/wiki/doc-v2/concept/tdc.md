+++
title="Toile de confiance sur Duniter V2"
# date = 2024-10-17
weight = 20
sort_by = "weight"

# [taxonomies]
# authors = ["Matograine",]
+++



# Toile de confiance sur Duniter V2

La monnaie libre doit être créée par chaque individu. Il faut donc identifier un compte à un individu. Dans la monnaie Ğ1, c'est un dispositif technique de 'toile de confiance' ou 'toile relationnelle' qui tient ce rôle.

[Fonctionnement de la toile de confiance](@/wiki/toile-de-confiance/_index.md)

Voici le cycle de vie d'une identité :

- Un individu ne pourra pas créer d'identité seul. Il devra être invité par un membre, et cette invitation tiendra lieu de première certification.
- Cet individu devra ensuite *confirmer l'invitation*. Il choisira à ce moment le pseudonyme de son compte membre. 
- Les certifications devront être *émises* tous les 5 jours, car elles sont enregistrées immédiatement en blockchain. Précédemment, plusieurs certifications pouvaient être émises simultanément, et elles étaient *enregistrées* avec <span class="blockchain_param">5</span> jours d'intervalle.
- L'inscription du membre est *déterministe* et non *probabiliste* : si une identité remplit toutes les conditions, alors elle est inscrite en blockchain immédiatement.
- Au plus tous les ans, chaque identité devra faire l'objet d'un renouvellement d'adhésion, qui est en réalité une *réévaluation de la règle de distance*.
- Si le renouvellement d'adhésion n'est pas fait, l'identité perd son droit à co-créer le Dividende Universel, et elle a <span class="blockchain_param">un mois</span> pour faire un renouvellement d'adhésion valide. Après quoi, l'identité est révoquée automatiquement.
- Le membre peut à tout moment révoquer son identité

[En pratique](@/wiki/doc-v2/utiliser/etre-membre.md) : Explication des différentes opérations effectuées durant la vie d'un compte membre.