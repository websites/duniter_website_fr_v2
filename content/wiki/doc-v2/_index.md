+++
title="Duniter v2"
# date = 2024-06-28
weight = 20
sort_by = "weight"

# [taxonomies]
# authors = ["JossRendall",]

[extra.translations]
en = "wiki/duniter-v2/"
+++

# Duniter v2

Duniter v2 est une réécriture de Duniter v1 avec le framework Substrate avec l’objectif de migrer la monnaie Ğ1.
Ce nouveau logiciel est actuellement en cours de développement et doit être testé.
Vous trouverez ici, la documentation utilisateur extraite du dépôt Git de Duniter ainsi que des explications générales.

- Dépôt Git [https://git.duniter.org/nodes/rust/duniter-v2s/](https://git.duniter.org/nodes/rust/duniter-v2s/)
- Forum Duniter section Développement [https://forum.duniter.org/c/dev/duniter-v2s/](https://forum.duniter.org/c/dev/duniter-v2s/57)
- Forum Duniter section Protocole [https://forum.duniter.org/c/protocols/g1v2proto/](https://forum.duniter.org/c/protocols/g1v2proto/60)
- Forum Duniter section Support [https://forum.duniter.org/c/support/duniter-v2/83](https://forum.duniter.org/c/support/duniter-v2/83)

Vous trouverez ci-dessous tout le nécessaire pour commencer à rejoindre le réseau Gdev, Blockchain provisoire, servant de support au développement et à l'adaptation de tous les clients (CésiumV2, Ginkgo, G1 SuperBot, etc...).

{% note(type="info") %}
Ce réseau Gdev n'est pas le réseau définitif.
{% end %}

Si vous voyez qu’il manque quelque chose, s’il vous plaît, venez dans la section support du [forum Duniter](https://forum.duniter.org/c/support/duniter-v2/83), cela nous aidera à améliorer les tutoriels et vous apporterez une contribution précieuse!

## Concepts

La compréhension des concepts de base de Duniter est une bonne étape avant l'installation de différents logiciel !

<div class="center-content"><a class="w3-button w3-teal w3-round" href="/wiki/doc-v2/concept/">💭 Concept</a></div>

## Installer

Une fois les concepts de base compris, vous pourrez être tentés d'installer les logiciels de l'infrastructure ou les logiciels clients.

<div class="center-content"><a class="w3-button w3-cyan w3-round" href="/wiki/doc-v2/installer/">🗳 Installer</a></div>
