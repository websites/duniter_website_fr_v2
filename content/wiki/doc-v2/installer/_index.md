+++
title="Installation des logiciels"
# date = 2024-10-17
weight = 20
sort_by = "weight"

# [taxonomies]
# authors = ["Matograine",]

[extra.translations]
en = "wiki/duniter-v2/"
+++

# Installation

Ce dossier regroupe les méthodes d'installation des logiciels utiles à la monnaie Ğ1. Certains logiciels peuvent avoir plusieurs méthodes d'installation suivant le support.

## Prérequis

Si vous débutez dans l'hébergement, vous trouverez ici des ressources sur les notions de bases nécessaire.

- [🥇 Prérequis généraux sur l'hébergement serveur](@/wiki/doc-v2/installer/prerequis.md)
- [🐳 Informations spécifiques à Docker](@/wiki/doc-v2/installer/debuter-avec-docker.md)
- [📝 Memo sur des actions courantes](@/wiki/doc-v2/installer/memo/_index.md)

## Infrastructure

Pour participer à l'infrastructure serveur qui permet à la monnaie de fonctionner.

- [🪞 installer un noeud miroir](@/wiki/doc-v2/installer/miroir/_index.md)
- [⚒️ installer un noeud forgeron](@/wiki/doc-v2/installer/forgeron/_index.md)
- [squid (manquant)]

## Clients

Pour installer des logiciels clients et interagir avec le réseau.

- [🖥️ installer Ğcli](@/wiki/doc-v2/installer/gcli/_index.md)

## Services utiles

- Aucune documentation pour le moment (Ğecko-web, Duniter-panel, etc.)