+++
title="Louer et configurer un nom de domaine"
# date = 2024-06-28
weight = 20
sort_by = "weight"

# [taxonomies]
# authors = ["JossRendall",]
+++



# Louer et configurer un nom de domaine

[TOC]

Si vous avez déjà un serveur, ce serveur a une adresse IP pour être contacté ur internet, qui ressemble par exemple à `131.2.42.65`. Louer un nom de domaine permet d'attribuer à cette adresse IP une adresse plus lisible par un humain, comme `duniter.org`.

## Louer un nom de domaine

Un nom de domaine se loue pour une durée limitée (même si vous lirez souvent "acheter" un nom de domaine), auprès d'une entreprise qu'on appelle le *registrar*. Suivant le registrar que vous choisirez, les prix peuvent varier, et les avantages également. Souvent, les registrars font une remise, parfois importante, pour la première année : ne vous laissez pas avoir, comparez les prix normaux et pas les offres. Par ailleurs, certains registrars vous offrent une adresse email et un espace web lors de la location d'un nom de domaine, ça peut être une occasion pour vous de délaisser les services gratuits des géants du web.

Il est également possible de mutualiser les coûts en louant un nom pour plusieurs personnes. Vous pourrez configurer des sous-domaines en `julie.notredomaine.fr`, `martine.notredomaine.fr`, etc. Ayez simplement confiance dans la personne qui administre le nom de domaine.

Voici une liste que nous recommandons :

|registrar|Avantages|
|:----|:---|
|[OVH](https://www.ovhcloud.com/fr/domains/)|Prix réduit sur les domaines en `.ovh`. Adresse email offerte. Première année offerte si vous louez déjà un serveur chez eux.|
|[Infomaniak](https://www.infomaniak.com/fr/domaines)|adresse email offerte. Accès à des outils en ligne. Hébergement d'un site minimal offert. (pas un serveur, un site)|

Nous vous recommandons de :
- louer le domaine pour une durée qui correspond à ce que vous prévoyez (1an, 2 ans, etc.)
- mettre en place un renouvellement automatique si le registrar le permet (entraîne souvent un surcoût), ou mettre un rappel dans votre agenda pour renouveler le domaine quelques jours/semaines avant son expiration.

Par ailleurs, les données (nom, adresse) du propriétaire d'un nom de domaine doivent être publiques. Pour éviter de voir vos données publiées, les registrars proposent une option 'confidentialité du domaine' qui permet de masquer ces données. Ceci entraîne également un surcoût.

Evidemment, pour louer le nom de domaine, vous devrez ouvrir un compte chez le registrar. Il est donc souvent pertinent de prendre le nom de domaine là où vous louez votre serveur, ce qui vous permet parfois d'avoir des réductions.

Suivant le registrar, la procédure d'enregistrement n'est pas tout à fait la même, mais à la fin vous devez avoir :
- un compte chez le registrar
- dans votre interface, un accès aux paramètres pour votre nom de domaine.

## Configurer la zone DNS

Dans cette partie, nous allons lier l'adresse IP de votre serveur à votre nom de domaine.
Les interfaces sont différentes suivant le registrar. Nous allons donc vous indiquer le principe avant de vous donner un exemple.

1. Trouver la section nom de domaine
   Il est rare que le registrar ne fournisse que le service nom de domaine. Dans votre interface, trouvez une section qui va s'appeler 'Domaine', 'Nom de domaine', ou quelque chose d'approchant. Elle sera au même niveau que par exemple 'hébergement web', 'Cloud', etc.
2. Trouver le paramétrage du nom de domaine
   La section 'Nom de domaine' va probablement vous afficher une liste des noms de domaine que vous louez chez ce registrar. A priori vous n'en avez qu'un. Cliquez sur la ligne ou sur une icône 'Paramètres' ou 'configurer'. Vous devez avoir accès aux informations concernant ce nom de domaine, et à différentes parties de configuration.
3. Trouver la `zone DNS`
   La partie qui nous intéresse s'appelle la 'zonde DNS' ou 'DNS zone'. Ce terme est assez standard, donc vous devriez le retrouver quel que soit votre registrar. Cliquez dessus.
4. Configurer la zone DNS
   La zone DNS vous permet d'éditer des *enregistrements DNS*, qui sont des informations liées à votre nom de domaine et publiées pour indiquer au monde comment il faut faire pour contacter ce nom de domaine. Certains enregistrements DNS sont probablement déjà remplis par votre registrar, n'y touchez pas.
5. Ajouter un domaine pour le noeud
   Dans cette interface, vous devriez trouver un bouton 'Nouvel enregistrement' ou 'ajouter'. Cliquez dessus. 
   
Une fois trouvé l'ajout d'un enregistrement DNS, vous devez indiquer :

- le type d'enregistrement (ceux qui nous intéressent sont A pour IPv4 et AAAA pour IPv6)
- le sous-domaine. Nous vous conseillons d'affecter un sous-domaine pour votre noeud, le domaine principal étant sans doute utilisé par le registrar pour les options qu'il vous propose. Si votre domaine est `mondomaine.net`, vous pouvez indiquer `g1.mondomaine.net`, `gdev.mondomaine.net`, ce que vous voulez.
- l'adresse IP. Elle vous a été fournie par votre hébergeur. Indiquez l'IPv4 pour un enregistrement A et l'IPv6 pour un enregistrement AAAA. Vous pouvez configurer une IPv4 et une IPv6 pour le même sous-domaine.
- Le TTL ('Time to live'). C'est un paramètre essentiellement technique, vous pouvez laisser la valeur par défaut.

Puis cliquez sur 'Ajouter' ou 'Enregistrer'. Les enregistrements IPv4 et IPv6 doivent être faits séparément. Une fois que vous avez fait tout cela, votre registrar va propager ces informations, ce qui n'est pas immédiat. 

## Vérifier la configuration du sous-domaine.

Pour vérifier que votre nom de domaine est bien enregistré, vous pouvez utiliser la ligne de commande avec l'outil `dig` :
```bash
dig g1.mondomaine.net
```
Dans la réponse, vous devez avoir la partie 'Answer section' qui vous donnera votre IP :
```
;; ANSWER SECTION:
g1.mondomaine.net.	3455	IN	A	133.2.42.65
```

## Exemple chez Infomaniak

Chez Infomaniak, on trouve la section nom de domaine via le menu 'Hamburger' qui liste les différentes applications.

<div class="center-content"><img alt="Capture d'écran de l'interface Infomaniak. Menu 'hamburger' ouvert. Il contient un lien 'Domain' sur lequel il faut cliquer." src="/imgs-doc-v2/memo/configurer-dns/interface-infomaniak.gif" width="50%"/></div>

Dans la section des noms de domaine, on peut sélectionner le nom de domaine qu'on veut gérer (à priori vous n'en avez qu'un).

<div class="center-content"><img alt="Capture d'écran d'une ligne de la liste des domaines, dans l'interface Infomaniak. Il faut cliquer dessus." src="/imgs-doc-v2/memo/configurer-dns/interface-infomaniak-2.gif" width="50%"/></div>

Sur l'interface de gestion de ce nom de domaine, on trouve la partie 'Zone DNS'.

<div class="center-content"><img alt="Capture d'écran de l'interface Infomaniak. La partie correcpondant au domaine qu'on veut gérer. Un trait rouge souligne le lien vers 'DNS Zone'." src="/imgs-doc-v2/memo/configurer-dns/interface-infomaniak-3.gif" width="50%"/></div>

Une fois dans la configuration de la zone DNS, on clique sur 'Ajouter un enregistrement'.

<div class="center-content"><img alt="Capture d'écran de l'interface Infomaniak, dans la gestion de la zone DNS. On voit une liste d'enregistrements DNS sur g1cotis.fr et un enregistrement qui définit un sous-domaine en www.g1cotis.fr. Un trait rouge indique le bouton 'Add a record'." src="/imgs-doc-v2/memo/configurer-dns/interface-infomaniak-4-zone-DNS.gif" width="50%"/></div>

Ici on ajoute un enregistrement 
- de type 'A' (pour une ipv4)
- pour le sous-domaine mon-noeud.g1cotis.fr
- qui pointe vers l'IP 131.2.42.65
- ayant un Time To Live (TTL) de 1h

On voit aussi que l'interface propose de configurer l'enregistrement ipv6 correspondant.

Pour terminer, on clique sur le bouton Save.

<div class="center-content"><img alt="Capture d'écran de l'interface Infomaniak, dans la gestion de la zone DNS. Ecran d'ajout d'un enregistrement." src="/imgs-doc-v2/memo/configurer-dns/interface-infomaniak-4-ajout-DNS.gif" width="50%"/></div>