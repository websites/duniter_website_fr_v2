+++
title = "Memo"
weight = 110
+++

# Memo

- [Configurer un nom de domaine](@/wiki/doc-v2/installer/memo/configurer-nom-de-domaine.md)
- [Configurer un pare feu](@/wiki/doc-v2/installer/memo/configurer-parefeu.md)
- [Diagnostiquer l'accessibilité du noeud](@/wiki/doc-v2/installer/memo/verifier-noeud-accessible.md)