+++
title="Vérifier l'accessibilité du noeud"
# date = 2024-06-28
weight = 20
sort_by = "weight"

# [taxonomies]
# authors = ["matograine",]
+++



# Vérifier l'accessibilité du noeud depuis l'extérieur

Il y a deux façons complémentaires de vérifier l'accessibilité du noeud.
Utilisez au moins l'une des trois, et si possible les trois.

[TOC]

## La télémétrie Polkadot

Rendez vous sur la page de [télémétrie Polkadot](https://telemetry.polkadot.io/#list/0xc184c4ccde8e771483bba7a01533d007a3e19a66d3537c7fd59c5d9e3550b6c3).
Le nom de votre noeud doit apparaître dans la liste.
Vous pouvez taper `Ctrl + F` pour ouvrir la recherche, et chercher le nom du noeud.

## Avec Gcli

Sur votre poste, installez [gcli](https://git.duniter.org/clients/rust/gcli-v2s/-/releases).
Il n'y a pas à ma connaissance de paquet Windows, donc si votre poste est sous Windows, vous pouvez passer cette partie.
Puis contactez votre noeud.

Par exemple, si votre noeud est sur `g1.mondomaine.net` :
```bash
gcli -u wss://g1.mondomaine.fr/ blockchain current-block
```

ou s'il n'a pas de nom de domaine et pas de certificat TLS (il est par exemple sur l'IP 42.42.42.42) :
```bash
gcli -u ws://42.42.42.42:9944 blockchain current-block
```

On doit obtenir quelque chose comme ça :
```
on wss://g1.mondomaine.fr/
finalized block	2208389
current block	2295714
```
