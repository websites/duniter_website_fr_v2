+++
title="Configurer le parefeu"
# date = 2024-10-18
weight = 20
sort_by = "weight"

# [taxonomies]
# authors = ["Matograine",]
+++



# Comment configurer le parefeu pour un noeud miroir

## Ouvrir les ports

Les "ports" sont des interfaces numérotées qui permettent de communiquer avec l'ordinateur.

Par analogie, c'est comme si votre ordinateur avait des milliers de portes pour y entrer.
Il peut aller contacter d'autres ordinateurs en ouvrant des portes (*ports sortants*) pour envoyer des messagers.
Mais on ne veut pas que des messagers extérieurs entrent dans l'ordinateur sans contrôle.
Donc toutes les portes doivent être fermées par défaut, sauf celles derrière lesquelles il y a un gardien (*ports entrants*) (les "gardiens" étant des logiciels qui utilisent les demandes qui entrent par un certain port).
Fin de l'analogie.

Les ports sont ouverts ou fermés grâce à un logiciel qu'on appelle un *pare-feu* (ou *firewall*). Nous allons utiliser `ufw`, qui est un des plus faciles d'utilisation.

Dans cet exemple (typique d'un noeud miroir simple), nous allons configurer votre ordinateur pour :
- Fermer tous les ports par défaut
- ouvrir le port SSH (22 par défaut, mais vous l'avez peut-être changé)
- ouvrir les ports 80 et 443, qui correspondent aux communications http et https
- ouvrir le port 30333, qui est le port nécessaire à la communiation peer-to-peer de votre noeud (vous avez peut-être modifié cette valeur)

Cette partie trouve sa source dans [cet article](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-with-ufw-on-ubuntu-20-04-fr) plus complet, mais avec beaucoup d'informations inutiles pour nous.

Commençons par installer ufw :
```bash
sudo apt-get update ; sudo apt-get install ufw
```

Puis bloquons tous les ports entrants, et autorisons les ports sortants :
```bash
sudo ufw default deny incoming
sudo ufw default allow outgoing
```

Puis ouvrons le port SSH:
```bash
## port par défaut
sudo ufw allow 22
## ou port personnalisé (ici 49999 en exemple)
sudo ufw allow 49999
```

Maintenant que le port SSH est ouvert, on peut activer ufw.
**Ne faites pas ceci avant d'avoir ouvert votre port SSH, sinon vous perdrez tout accès à votre serveur et vous devrez demander une réinstallation à votre fournisseur, puis recommencer depuis le début.**
```bash
sudo ufw enable
```

Enfin, ouvrons les autres ports :
```bash
## Les ports web
sudo ufw allow 80
sudo ufw allow 443
## La communication entre noeuds (vous avez peut-être une valeur personnalisée)
sudo ufw allow 30333
```

Pour vérifier les règles, utilisez `ufw status` :
```bash
sudo ufw status
```

On doit obtenir :
```bash
Status: active

To                         Action      From
--                         ------      ----
49999                      ALLOW       Anywhere                  
30333                      ALLOW       Anywhere                  
80                         ALLOW       Anywhere                  
443                        ALLOW       Anywhere                  
49999 (v6)                 ALLOW       Anywhere (v6)             
30333 (v6)                 ALLOW       Anywhere (v6)             
80 (v6)                    ALLOW       Anywhere (v6)             
443 (v6)                   ALLOW       Anywhere (v6)
```

Si vous souhaitez supprimer une règle, par exemple pour fermer le port 80, vous allez dire à ufw de supprimer la règle 'allow 80'. Ceci se fait ainsi :
```bash
sudo ufw delete allow 80
```