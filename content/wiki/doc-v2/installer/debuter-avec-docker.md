+++
title="Docker ? Kesako"
# date = 2024-06-28
weight = 1
sort_by = "weight"

# [taxonomies]
# authors = ["JossRendall",]
+++




# Docker ? Kézako ?

[TOC]

Docker est une plateforme permettant de lancer certaines applications dans des conteneurs logiciels.
Nous n’allons pas vous donner une formation complète sur Docker mais des éléments de compréhension de cet outil et de ses bases afin de mieux le comprendre.

Docker est un outil qui peut empaqueter une application et ses dépendances dans un conteneur isolé, qui pourra être exécuté sur n’importe quel serveur. Il ne s’agit pas de virtualisation, mais de conteneurisation, une forme plus légère qui s'appuie sur certaines parties de la machine hôte pour son fonctionnement.

Cette approche permet d’accroître la flexibilité et la portabilité d’exécution d’une application, laquelle va pouvoir tourner de façon fiable et prévisible sur une grande variété de machines hôtes, que ce soit sur la machine locale, un cloud privé ou public, une machine nue, etc.

## La notion de conteneur Docker

L’objectif d’un conteneur est le même que pour un serveur dédié virtuel : héberger des services sur un même serveur physique tout en les isolant les uns des autres.

Un conteneur est cependant moins figé qu’une machine virtuelle en matière de taille de disque et de ressources allouées.

Un conteneur permet d’isoler chaque service : le serveur web, la base de données, des applications pouvant être exécutées de façon indépendante dans leur conteneur dédié, contenant uniquement les dépendances nécessaires. Chaque conteneur peut être relié aux autres par des réseaux virtuels.

Il est possible de monter des volumes de disque de la machine hôte dans un conteneur. Si aucun processus n’est démarré dans le conteneur, alors celui-ci s’arrête.

On parle parfois de virtualisation d’OS : contrairement à la virtualisation qui émule par logiciel différentes machines sur une machine physique, la conteneurisation émule différents OS sur un seul OS.

## Liste des éléments principaux utilisés par un conteneur

### Image

Qu’est-ce qu’une image Docker ?

Les images Docker sont bien plus que de simples paquets logiciels.

Imaginez-les comme des boîtes magiques qui renferment tout ce dont une application a besoin pour fonctionner, du code source aux dépendances, le tout encapsulé dans un ensemble léger et autonome. Ces images servent de fondation à la technologie de conteneurisation, permettant aux développeurs de créer des environnements d’exécution cohérents, indépendamment des variations du système hôte et donc elles offrent une solution élégante aux défis de la gestion des dépendances logicielles et de la portabilité des applications.

Dans notre exploration de l’univers Docker, il est essentiel de comprendre cette distinction fondamentale.

Une image est un package qui contient tout ce dont une application a besoin pour fonctionner, tandis qu’un conteneur est une instance en cours d’exécution de cette image, offrant une isolation et une portabilité.

Le processus d’obtention d’images Docker depuis un référentiel, une étape cruciale dans le déploiement et la gestion d’applications conteneurisées.
Les référentiels Docker, tels que Docker Hub, abritent un éventail d’images prêtes à l’emploi, couvrant tout, des serveurs web aux bases de données.

Lien vers la source officielle des images devant être utilisées pour faire fonctionner La Monnaie Libre :

[https://hub.docker.com/u/duniter](https://hub.docker.com/u/duniter)

Lien vers le « repositorie » de Duniter v2 utilisant la Blockchain Gdev :

[https://hub.docker.com/r/duniter/duniter-v2s-gdev/tags](https://hub.docker.com/r/duniter/duniter-v2s-gdev/tags)

### Volume

Il est essentiel de comprendre que les données stockées dans un conteneur Docker peuvent être éphémères.

Un conteneur peut être stoppé, supprimé, ou redémarré à tout moment, emportant avec lui les données qu’il contient. Cela signifie que les bases de données et les applications avec état nécessitent une attention particulière. Si nous ne prenons pas les mesures appropriées, ces données disparaîtront à chaque redémarrage ou suppression du conteneur.

Grâce aux volumes gérés par Docker, les données sont stockées dans une partie du système de fichiers hôte, généralement sous « /var/lib/docker/volumes/ » sous Linux, assurant ainsi leur persistance après la suppression du conteneur.

En termes simples, un volume Docker est un répertoire ou un espace de stockage dédié qui peut être partagé entre les conteneurs et persiste même lorsque le conteneur associé est arrêté ou supprimé.

Cette fonctionnalité fait des volumes un outil essentiel pour la gestion des données dans un environnement Docker.

### Réseaux docker

Imaginez un monde où vos applications s’exécutent dans des conteneurs isolés, comme des îles dans un archipel.
Chaque île est autonome, avec ses propres ressources et son propre environnement. Mais que faire si vous souhaitez que vos applications communiquent entre elles ? C’est là que les réseaux Docker entrent en jeu.

Les réseaux Docker fournissent un moyen pour les conteneurs de communiquer entre eux, que ce soit sur une même machine hôte ou sur des machines hôtes différentes dans un environnement distribué. Ces réseaux permettent d’isoler les communications entre les conteneurs, offrant ainsi une couche de sécurité supplémentaire. Autrement ils sont comme des ponts qui relient vos îles conteneurisées.

Ils permettent aux conteneurs de communiquer de manière transparente, comme s’ils étaient sur le même réseau physique.

Les réseaux Docker ont révolutionné la façon dont nous gérons les conteneurs. Ils ont rendu la communication entre conteneurs simple, efficace et sécurisée.

Parmi les problèmes que les réseaux Docker résolvent :
- **Isolation** : Les conteneurs sont isolés par défaut, ce qui signifie qu’ils ne peuvent pas communiquer entre eux sans configuration supplémentaire.
- **Connectivité** : Ils permettent aux conteneurs de communiquer de manière transparente, comme s’ils étaient sur le même réseau physique.
- **Sécurité** : Ils peuvent être segmentés pour limiter l’accès aux ressources et protéger vos applications contre les attaques.
- **Simplicité** : Ils sont faciles à configurer et à gérer, ce qui vous permet de gagner du temps et de vous concentrer sur vos applications.

En résumé, les réseaux Docker sont un élément essentiel de la technologie des conteneurs.

Ils permettent de connecter vos conteneurs, ce qui vous permet de créer des applications distribuées plus puissantes et plus flexibles.

### Fichier compose

Docker Compose offre une solution simple et efficace pour orchestrer et gérer les applications conteneurisées.

Il s’agit d’une extension de Docker qui utilise des fichiers de configuration YAML, généralement nommés « docker-compose.yml », pour définir les services, les réseaux, les volumes et d’autres ressources indispensables à une application.

La structure d’un fichier compose est très importante et doit être respectée afin que celui-ci soit compris par Docker.

Ci-après, un exemple de format YAML, pour docker-compose, destiné à créer un conteneur faisant tourner un nœud miroir.

```yml
version: "3.5"

services:
  duniter-v2s-mirror:
    image: duniter/duniter-v2s-gdev:latest
    container_name: duniter-v2s-mirror
    restart: unless-stopped
    ports:
      # Prometheus endpoint
      - 9615:9615
      # rpc via websocket
      - 9944:9944
      # p2p
      - 30333:30333
    volumes:
      - data-mirror:/var/lib/duniter/
    environment:
      - DUNITER_CHAIN_NAME=gdev
      - DUNITER_NODE_NAME=Toto-Gdev-Mirror
    networks:
      - npm-nw

volumes:
  data-mirror:

networks:
  npm-nw:
    external: true
```
Grâce à cet exemple, nous pouvons remarquer qu’il y a plusieurs sections principales dans le fichier.

- **version** : Indique la version du format de fichier Docker Compose utilisée.
- **services** : Définis les services qui composent l’application. Chaque service est défini par un nom et une configuration.
- **image** : Définis l’image qui sera utilisée par le conteneur
- **ports** : Définis les ports qui seront utilisés par le conteneur
- **volumes** : Définis les volumes de données persistants utilisés par les services.
- **environment** : Définis les variables et leurs valeurs utilisées par le conteneur.
- **networks** : Définis les réseaux utilisés par les services.

## Conclusion

Avec les éléments décrits ci-dessus, vous avez maintenant de quoi mieux comprendre l'environnement Docker et où appliquer les réglages qui vous seront demandés pour faire tourner un Noeud Duniter.
