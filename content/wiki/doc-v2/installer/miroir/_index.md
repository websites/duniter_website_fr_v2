+++
title="Installer un noeud miroir"
# date = 2024-10-17
weight = 20
sort_by = "weight"

# [taxonomies]
# authors = ["JossRendall","Matograine"]

[extra.translations]
en = "wiki/duniter-v2/run-mirror/"
+++



## Monter un Noeud Mirroir

Un nœud miroir est un nœud auquel le Juniste se connecte grâce à son logiciel client via l’API RPC.
L’exécution d’un nœud miroir Duniter est utile pour la redondance du réseau, elle permet de partager la charge entre différents serveurs et d’augmenter la résilience aux pannes. Un noeud miroir ne va pas calculer de blocs.

C’est également la première étape technique à maitriser avec aisance avant de vouloir devenir Smith et forger des blocs.

A la date du 28 juin 2024, suivant les informations disponibles et obtenues auprès des développeurs, voici les caractéristiques principales que doit avoir la machine et les compétences minimales à maitriser pour rejoindre le réseau Gdev:
- la machine doit être connectée au réseau internet 24h/7j
- système d'exploitation basé sous Linux (Debian Serveur conseillé)
- savoir gérer l'ouverture de ports sur votre machine et/ou votre Box internet
- Duniter-v2s est actuellement disponible sous Docker (conseillé) et en paquet Debian. <!--et pour [Yunohost](https://yunohost.org/fr) en installation manuelle.-->

### Modes d'installation

- [Installer et configurer un noeud miroir sous Docker](@/wiki/doc-v2/installer/miroir/docker.md)
- [Installer et configurer un noeud miroir sous Debian](@/wiki/doc-v2/installer/miroir/debian.md)
<!-- - [Installer et configurer un noeud miroir avec Yunohost](@/wiki/doc-v2/installer/miroir/yunohost.md) -->
