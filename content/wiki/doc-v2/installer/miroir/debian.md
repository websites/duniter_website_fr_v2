+++
title="Miroir Debian"
# date = 2024-06-28
weight = 20
sort_by = "weight"

# [taxonomies]
# authors = ["matograine",]
+++



# Installer et configurer un noeud miroir sous Debian

Ce document vous indique les différentes étapes pour installer un noeud miroir sous Debian.
Certaines étapes sont documentées sur des pages annexes : les prérequis et la mise en place du reverse-proxy.
Nous vous conseillons de prendre votre temps et de bien faire chaque étape.

[TOC]

## Prérequis

Ce tutoriel suppose que :
- vous savez utiliser la [ligne de commande](@/wiki/doc-v2/installer/prerequis.md#utiliser-la-ligne-de-commande)
- vous avez [un serveur installé sous Debian](@/wiki/doc-v2/installer/prerequis.md#louer-un-serveur)
- vous pouvez vous y connecter en [ssh](@/wiki/doc-v2/installer/prerequis.md#se-connecter-en-SSH-a-son-serveur)
- vous avez fait une [sécurisation de base du serveur](@/wiki/doc-v2/installer/prerequis.md#la-securite-de-base-du-serveur)

Les paquets que nous allons télécharger sont pour des serveurs ayant une architecture amd64. Ceci fonctionnera pour les serveurs loués, et pour un serveur à la maison s'il a une architecture amd64. Mais pour des serveurs ARM (typiquement un raspberry pi), ces fichiers ne fonctionneront pas, il vous faudra compiler Duniter.

Connectez-vous à votre serveur en SSH.


## Mise en place

On va créer un utilisateur pour Duniter, puis se placer dans son dossier home
```bash
sudo adduser duniter
cd /home/duniter
```

On va créer un dossier spécifique histoire que nos fichiers soient bien rangés :
```bash
mkdir duniter
cd duniter
```

## Installer


Vous allez télécharger la dernière version du paquet Debian pour Duniter :

> NB - ce lien de téléchargement est le seul que j'ai trouvé pour le moment (1710/2024). Il n'est peut-être pas le paquet le plus récent.

```bash
## télécharger avec Curl
curl https://ipfs.io/ipfs/QmQrEHjXS4uuQuLwhQxtPTigygP6nyU747LCYRs1B4cg5T?filename=duniter_0.8.0-1_amd64.deb.tar.gz -o artifacts.zip

## OU télécharger avec wget
wget https://ipfs.io/ipfs/QmQrEHjXS4uuQuLwhQxtPTigygP6nyU747LCYRs1B4cg5T?filename=duniter_0.8.0-1_amd64.deb.tar.gz
```

ensuite on ouvre ce qu'on a téléchargé :

```bash
## dézipper
tar -xzvf duniter_0.8.0-1_amd64.deb.tar.gz
## se placer dans le bon dossier
ls
## La commande ls doit nous donner le nom du paquet Debian, par exemple : `duniter_0.8.0-1_amd64.deb`
```

enfin on installe le paquet.

La commande est précédée de *sudo* car elle nécessite les droits administrateur.
Votre mot de passe devrait vous être demandé (si ça n'est pas le cas, c'est que vous êtes déjà administrateur, c'est normal)

```bash
## ici on utilise le caractère * qui veut dire 'joker'.
## Si vous avez plusieurs fichiers d'installation Duniter .deb, précisez lequel vous voulez installer.
sudo dpkg -i duniter_*_amd64.deb
```

## Configurer le noeud

Une fois installé, le fichier de configuration devrait se trouver dans `/etc/duniter/env_file`.
Nous allons modifier ce fichier avec l'éditeur `nano`. On se déplace dans `nano` avec les flèches et pas avec la souris.

```bash
sudo nano /etc/duniter/env_file
```

On y trouve plusieurs paramètres :

- `DUNITER_NODE_NAME` : Le nom de votre noeud.
  Nommez-le comme vous voulez, mais sans caractères spéciaux ou accents, et sans espace.
  Il doit être unique sur le réseau, donc si vous avez plusieurs noeuds, nommez les par exemple martin-1, martin-2, etc
- `DUNITER_CHAIN_NAME` : Le nom du réseau que vous rejoignez. Utilisez `gdev` ou `gtest` pour un des réseaux de test, ou `g1` pour le réseau de production.
- `DUNITER_LISTEN_ADDR` : L'adresse à laquelle les autres noeuds vont communiquer avec le vôtre. Vous pouvez la laisser à `/ip4/0.0.0.0/tcp/30333`.
  - NB : si vous modifiez le port (la valeur 30333), notez bien la valeur que vous utilisez car il faudra ouvrir ce port sur le parefeu
  - NB : ne modifiez pas l'adresse IP (la valeur 0.0.0.0)
  - NB : si votre serveur est accessible uniquement en IPV6, l'adresse par défaut doit être `/ip6/::/tcp/30333`
- `DUNITER_RPC_CORS` : Ce paramètre définit quelle page web peut faire des requêtes vers votre noeud. Pour créer un noeud miroir public, sa valeur doit être `all`. Si vous hébergez un service sur le site https://monservice.net et que vous voulez avoir un noeud réservé à ce service, la valeur doit être `https://monservice.net*`. Plusieurs valeurs peuvent être associées, séparées par des virgules (voir l'exemple donné dans le document)
- `DUNITER_PRUNING_PROFILE` : Ce paramètre indique après combien de blocs finalisés votre noeud 'oublie' les blocs précédents. Pour un noeud mirois normal, la valeur par défaut `256` est suffisante. Pour un noeud d'archive (nécessaire pour un datapod), il faut mettre la valeur `archive`. Ceci consommera beaucoup d'espace disque, car il stockera tout l'historique de la blockchain.
- `BASE_PATH`: L'emplacement où Duniter stockera ses données. La valeur par défaut convient. Si vous mettez en place un noeud archive, il faudrait que cet emplacement soit sur une partition large et séparée du système, pour éviter que Duniter prenne toute la place et ne vous crée des problèmes.
- `ORACLE_RPC_URL` : ce paramètre ne sert pas pour un noeud miroir.
- `ORACLE_LOG_LEVEL` : ce paramètre ne sert pas pour un noeud miroir.

Vous pouvez enregistrer vos modifications en cours de route en faisant `Ctrl + O`.

A la fin, sortez de nano avec `Ctrl + X`.

## Lancer le service Duniter

Votre noeud est prêt à être lancé !
Pour cela, on va indiquer à Debian de le lancer en tant que service, pour qu'il le redémarre automatiquement :
```bash
## démarrer le service
sudo systemctl start duniter-mirror.service
## activer le service au démarrage
sudo systemctl enable duniter-mirror.service
```

vous pouvez voir les logs de votre noeud avec journalctl :
```bash
## en différé 
## Par exemple pour les envoyer à quelqu'un qui vous les demande, pour diagnostiquer un problème
journalctl -u duniter-mirror

## en direct
## Faites Ctrl + X pour sortir
journalctl -ef -u duniter-mirror

## sur une période
## ici le 15/10/2024 de 10h à 15h
journalctl -u duniter-mirror --since '2024-10-15 10:00:00' --until '2024-10-15 15:00:00'
```

Vous devriez voir ce genre de lignes :
```bash
duniter-mirror_1  | 2024-06-30 09:29:39 Duniter    
duniter-mirror_1  | 2024-06-30 09:29:39 ✌️  version 0.0.0-unknown    
duniter-mirror_1  | 2024-06-30 09:29:39 ❤️  by librelois <c@elo.tf>:tuxmain <tuxmain@zettascript.org>:c-geek <https://forum.duniter.org/u/cgeek>:HugoTrentesaux <https://trentesaux.fr>:bgallois <benjamin@gallois.cc>:Duniter Developers <https://duniter.org>:Axiom-Team Developers <https://axiom-team.fr>, 2021-2024    
duniter-mirror_1  | 2024-06-30 09:29:39 📋 Chain specification: ĞDev    
duniter-mirror_1  | 2024-06-30 09:29:39 🏷  Node name: matograine    
duniter-mirror_1  | 2024-06-30 09:29:39 👤 Role: FULL    
duniter-mirror_1  | 2024-06-30 09:29:39 💾 Database: ParityDb at /var/lib/duniter/chains/gdev/paritydb/full    
duniter-mirror_1  | 2024-06-30 09:29:44 👶 Creating empty BABE epoch changes on what appears to be first startup.    
duniter-mirror_1  | 2024-06-30 09:29:44 Local node identity is: 12D3KooWLrqPZXz4uFuAvnGcXuZDzsWhEvs8XQcavAmVyg8QyUiJ    
duniter-mirror_1  | 2024-06-30 09:29:44 Running litep2p network backend    
duniter-mirror_1  | 2024-06-30 09:29:44 💻 Operating system: linux    
duniter-mirror_1  | 2024-06-30 09:29:44 💻 CPU architecture: x86_64    
duniter-mirror_1  | 2024-06-30 09:29:44 💻 Target environment: gnu    
duniter-mirror_1  | 2024-06-30 09:29:44 💻 CPU: Intel Core Processor (Haswell, no TSX)    
duniter-mirror_1  | 2024-06-30 09:29:44 💻 CPU cores: 1    
duniter-mirror_1  | 2024-06-30 09:29:44 💻 Memory: 1935MB    
duniter-mirror_1  | 2024-06-30 09:29:44 💻 Kernel: 6.1.0-21-cloud-amd64    
duniter-mirror_1  | 2024-06-30 09:29:44 💻 Linux distribution: Debian GNU/Linux 11 (bullseye)    
duniter-mirror_1  | 2024-06-30 09:29:44 💻 Virtual machine: yes    
duniter-mirror_1  | 2024-06-30 09:29:44 📦 Highest known block at #0    
duniter-mirror_1  | 2024-06-30 09:29:44 〽️ Prometheus exporter started at 127.0.0.1:9615    
duniter-mirror_1  | 2024-06-30 09:29:44 Running JSON-RPC server: addr=0.0.0.0:9944, allowed origins=["*"]    
duniter-mirror_1  | 2024-06-30 09:29:44 ***** Duniter has fully started *****    
## ... Puis les lignes indiquant que le noeud sync
duniter-mirror_1  | 2024-07-14 07:59:36 ⚙️  Syncing, target=#2227610 (1 peers), best: #1002 (0xfc43…7938), finalized #512 (0x64a6…1fbb), ⬇ 204.1kiB/s ⬆ 0.4kiB/s
duniter-mirror_1  | 2024-07-14 07:59:41 ⚙️  Syncing 323.8 bps, target=#2227610 (7 peers), best: #2621 (0x186b…95a2), finalized #2560 (0x927d…63ed), ⬇ 110.8kiB/s ⬆ 1.9kiB/s
duniter-mirror_1  | 2024-07-14 07:59:46 ⚙️  Syncing 397.4 bps, target=#2227611 (7 peers), best: #4608 (0x081a…e9cc), finalized #4096 (0x6f24…586b), ⬇ 132.0kiB/s ⬆ 0.1kiB/s
duniter-mirror_1  | 2024-07-14 07:59:51 ⚙️  Syncing 306.0 bps, target=#2227612 (7 peers), best: #6138 (0x8380…c8d6), finalized #5632 (0x06cb…186a), ⬇ 98.6kiB/s ⬆ 0.3kiB/s
```

Il est essentiel d'avoir des lignes qui indiquent 'Syncing'. Si vous voyez des lignes comme celle ci-dessous, c'est que le noeud ne synchronise pas.
Notez 'best: #0' et '0 peers' : Il n'y a aucun bloc et aucun autre noeud contacté.
Il faut sans doute [ouvrir le port](@/wiki/doc-v2/installer/miroir/configurer-nginx.md) p2p (`30333` par défaut, celui que vous avez indiqué pour `DUNITER_LISTEN_ADDR`)
```bash
duniter-mirror_1  | 2024-06-30 05:36:33 💤 Idle (0 peers), best: #0 (0xc184…b6c3), finalized #0 (0xc184…b6c3), ⬇ 0 ⬆ 0
```

## Vérifier le bon fonctionnement du noeud

Si vous ne l'avez pas déjà fait, [installez Ğcli](@/wiki/doc-v2/installer/gcli/_index.md) sur votre serveur.

Puis regardez si le noeud est accessible en local :

```bash
gcli -u ws://localhost:9944 blockchain current-block
```
on doit obtenir :
```
on ws://localhost:9944
finalized block	2208389
current block	2295714
```


## Configurer le reverse-proxy

Maintenant que le noeud est fonctionel, il faut le rendre accessible depuis l'extérieur.
Pour cela, on met en place un *reverse proxy*, c'est-à-dire un logiciel qui va filtrer les requêtes transmises au noeud Duniter, mais qui va surtout chiffrer la communication du noeud avec les applications.

- [avec Nginx](@/wiki/doc-v2/installer/miroir/configurer-nginx.md)
- La documentation concernant Apache reste à écrire.

## Vérifier l'accessibilité du noeud depuis l'extérieur

Votre noeud est désormais installé et accessible de l'extérieur.
Si vous ne l'avez pas déjà fait, il faut [vérifier ce dernier point](@/wiki/doc-v2/installer/memo/verifier-noeud-accessible.md).


