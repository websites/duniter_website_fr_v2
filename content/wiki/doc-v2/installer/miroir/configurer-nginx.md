+++
title="Configurer NGinx"
# date = 2024-06-28
weight = 20
sort_by = "weight"

# [taxonomies]
# authors = ["JossRendall",]
+++



# Comment configurer nginx pour un noeud miroir

[TOC]

## Pourquoi NginX

NginX (prononcer N-jean-X) et Apache sont deux logiciels qui font serveur web + reverse proxy qui dominent le marché.
Les deux sont très utilisés, avec des fonctionnalités similaires.
Nginx est plus récent et est moins consommateur en ressources sur l'ordinateur.
Je le recommande si vous débutez dans ce domaine.
Si vous avez vos habitudes avec Apache, ou si vous installez votre noeud où Apache est déjà utilisé, utilisez la documentation concernant Apache. Apache et NginX ne peuvent pas être utilisés en même temps.

## Configurer le parefeu

Configurer le parefeu est nécessaire pour que l'extérieur puisse communiquer avec votre serveur.

Si vous souhaitez mettre en place un reverse-proxy, suivez la documentation pour la [mise en place du parefeu]([documentation](@/wiki/doc-v2/installer/memo/configurer-parefeu.md)) en ouvrant les ports :

- SSH (22 par défaut, mais vous l'avez peut-être changé)
- 80
- 443 si vous souhaitez mettre en place le chiffrement TLS
- 30333

## Installer et démarrer Nginx

[source](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-20-04-fr)

L'installation est très facile. On va d'abord s'asurer que Apache est désinstallé.

```bash
sudo apt update
sudo apt remove apache2
sudo apt install nginx
```

NginX devrait démarrer tout seul.

Pour le vérifier, si vous connaissez l'adresse IP de votre serveur (votre hébergeur devrait vous l'avoir fournie), rendez-vous via un navigateur sur `http://<adresse-ip>`. Vous devriez tomber sur une page qui vous dit "Welcome to Nginx".

On peut manipuler le service NginX de la façon suivante :

```bash
## Arréter le service
sudo systemctl stop nginx
## démarrer le service
sudo systemctl start nginx
## redémarrer le service
sudo systemctl restart nginx
## recharger la configuration
sudo systemctl reload nginx
## Connaître l'état du service
sudo systemctl status nginx
```

Par sécurité, on fait une sauvegarde de la configuration par défaut :
```bash
cp /etc/nginx/sites-available/default /etc/nginx/sites-available/default.BACKUP
```

## Vérifier que le nom de domaine pointe bien vers votre IP

Maintenant que votre serveur web est installé et fonctionne, vérifions que votre nom de domaine pointe bien sur votre serveur.

Si vous avez par exemple le nom de domaine g1.mondomaine.net, rendez-vous sur la page `http://g1.mondomaine.net`.
Vous devez voir apparaître la même page qui vous dit "Welcome to Nginx".

Si ce n'est pas le cas, il vous faut modifier la zone DNS de votre nom de domaine pour la faire pointer vers votre IP.
Utilisez ce [tutoriel pour cela](@/wiki/doc-v2/installer/memo/configurer-nom-de-domaine.md).

## Mettre en place un certificat TLS Let's Encrypt

Le certificat TLS est ce qui permet de chiffrer la communication de votre serveur avec l'extérieur.
Il se voit quand vous naviguez sur le web, certaines adresses sont en `http` (non chiffré) et d'autres en `https` (chiffré). 

Ce certificat vient d'une 'autorité', et la plupart des autorités vendent les certificats.
Fort heureusement pour nous, la fondation Mozilla a mis en place le programme *Let's Encrypt*, qui fournit des certificats gratuitement. Ce certificat a une durée de vie limitée et doit être renouvelé à intervalles réguliers, nous allons donc utiliser `certbot`, un outil pour le renouveler automatiquement.

Tout d'abord installer certbot :
```bash
sudo apt update
sudo apt install certbot
```

Puis, on va demander à certbot de générer un certificat (ici pour le domaine `g1.mondomaine.net`) :
```bash
certbot --nginx --cert-only g1.mondomaine.net
```
Si c'est la première fois que vous utilisez certbot, il va vous demander d'entrer votre adresse email et d'accepter les conditions d'utilisation.

certbot va vous indiquer quel est le chemin des fichiers `fullchain.pem` et `privkey.pem`, qui vont ressembler à ca. Notez-les bien.
```
/etc/letsencrypt/live/g1.mondomaine.net/fullchain.pem
/etc/letsencrypt/live/g1.mondomaine.net/privkey.pem
```

## Configurer le https pour WebSocket (wss)

Pour faire ceci, on va se placer en administrateur :
```bash
sudo -i
```

### Créer le fichier de configuration

Pour configurer le https pouv l'accès WebSocket à votre noeud, il faut nous placer dans `/etc/nginx/sites-available` :
```bash
cd /etc/nginx/sites-available
```

Ce répertoire contient toutes les configurations que vous *pourriez activer*.
Les conditions réellement actives sont dans `/etc/nginx/sites-enabled`.
Nous allons d'abord créer le fichier de configuration, puis créer un *lien* pour le faire apparaître dans `sites-enabled`.

Créons donc le fichier de configuration :
```bash
nano g1.mondomaine.net
```
l'éditeur de texte `nano` va s'ouvrir, vous pouvez y coller cette configuration :
```
# see http://nginx.org/en/docs/http/websocket.html
map $http_upgrade $connection_upgrade {
    default upgrade;
    ''      close;
}

server {
  server_name VOTRE_DOMAINE;

  listen 443 ssl http2;
  listen [::]:443 ssl http2;
  ssl_certificate CHEMIN/VERS/fullchain.pem;
  ssl_certificate_key CHEMIN/VERS/privkey.pem;

  root /nowhere;

  add_header X-Frame-Options SAMEORIGIN;
  add_header X-XSS-Protection "1; mode=block";
  proxy_redirect off;
  proxy_buffering off;
  proxy_set_header Host $host;
  proxy_set_header X-Real-IP $remote_addr;
  proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  proxy_set_header X-Forwarded-Proto $scheme;
  proxy_set_header X-Forwarded-Port $server_port;
  proxy_read_timeout 90;


  location / {
    proxy_pass http://localhost:9944;

    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection $connection_upgrade;
    proxy_http_version 1.1;

    proxy_read_timeout 1200s;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $host;
  }
}
```

Plusieurs valeurs sont à modifier :
- `VOTRE_DOMAINE` est à remplacer par votre nom de domaine (dans cet exemple `g1.mondomaine.net`)
- `CHEMIN/VERS/fullchain.pem` est à remplacer par le chemin vers fullchain.pem, que vous a donne certbot (dans cet exemple `/etc/letsencrypt/live/g1.mondomaine.net/fullchain.pem`)
- `CHEMIN/VERS/privkey.pem` est à remplacer par le chemin vers privkey.pem, que vous a donne certbot (dans cet exemple `/etc/letsencrypt/live/g1.mondomaine.net/privkey.pem`)
- `http://localhost:9944` est à laisser tel quel dans la plupart des cas. Si vous avez installé le noeud par Docker et que vous avez mappé le port 9944 de Docker avec un port spécifique sur le système, il faut remplacer `9944` par votre port spécifique.

Ensuite vous sauvegardez le fichier avec `Ctrl + O`, et vous quittez avec `Ctrl + X`.

### Activer la configuration

Nous allons maintenant créer un lien pour faitre apparaître notre fichier de configuration dans `/etc/nginx/sites-enabled`, grâce à la commande `ln` :
```bash
ln /etc/nginx/sites-available/g1.mondomaine.net /etc/nginx/sites-enabled/g1.mondomaine.net
```

On vérifie ensuite que le lien est bien créé :
```bash
ls /etc/nginx/sites-enabled/
```

On peut maintenant demander à NginX de vérifier que la configuration est bonne :
```bash
nginx -t
```
on doit obtenir :
```bash
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```

Puis on active réellement la configuration :
```bash
systemctl reload nginx.service 
```

## Vérifier que le noeud est accessible

Normalement, votre noeud est accessible depuis l'extérieur, avec l'utilisation du chiffrement TLS.
Nous vous invitons à utiliser [cette page](@/wiki/doc-v2/installer/memo/verifier-noeud-accessible.md) pour vérifier ceci.