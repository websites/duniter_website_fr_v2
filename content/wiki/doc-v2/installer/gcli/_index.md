+++
title="Installer gcli"
# date = 2024-10-17
weight = 60
sort_by = "weight"

# [taxonomies]
# authors = ["Matograine",]
+++




# Installer Ğcli

On va installer gcli, dans sa dernière version. Pour cela :

- Allez sur la page des versions : https://git.duniter.org/clients/rust/gcli-v2s/-/releases/
- Faites un clic-droit sur le lien qui indique 'Debian Package' et copiez le lien
- Ça devrait vous donner un lien du genre `https://git.duniter.org/clients/rust/gcli-v2s/-/jobs/artifacts/0.2.14/raw/target/debian/gcli_0.2.14-1_amd64.deb`.
  
Ensuite, on le télécharge et on l'installe :
```bash
## télécharger
wget <votre lien>
## Il y a un problème dans le lien, qui fait qu'il ne porte pas le bon nom... Il faut le renommer.
## On suppose qu'il n'y a qu'un fichier
bon_nom=`ls gcli_*.deb?* | sed 's/\?.*$//'`
mv gcli_*.deb?* $bon_nom
## installer
## s'il y a plusieurs fichiers, indiquez à dpkg le nom exact du fichier à installer (la dernière version)
sudo dpkg -i gcli_*_amd64.deb
```