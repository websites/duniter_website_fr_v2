+++
title="Prérequis"
# date = 2024-06-28
weight = 20
sort_by = "weight"

# [taxonomies]
# authors = ["JossRendall","matograine"]
+++



# Prérequis

Cette page regroupe des prérequis qui sont communs à toutes les installations.
Il pointe vers des ressources, parfois externes, qui pourront vous guider.
Si vous débutez, vous devrez faire toutes ces étapes  avant d'iinstaller un noeud à proprement parler.
Vous apprendrez beaucoup au passage !

[TOC]

## Utiliser la ligne de commande

La ligne de commande est le moyen privilégié pour administrer un serveur.

Au lieu d'avoir une interface graphique où on peut cliquer, on a une interface où il faut entrer du texte pour dire à l'ordinateur de faire une chose (lancer un logiciel, faire un calcul, afficher une information, etc.).
C'est une façon d'interagir avec un ordinateur qui est plus polyvalente qu'une interface graphique, plus puissante (on peut effacer tout son système si on ne fait pas attention), et plus standard. C'est parce qu'elle est plus standard que la plupart des tutoriels que vous trouverez concernant l'administration d'un serveur GNU/Linux donnent des instructions pour la ligne de commande.

Les postes sous GNU/Linux ou MacOS ont un terminal intégré, sur lequel vous pouvez faire des essais sans accéder à votre serveur.
Sous Windows, il y a également un terminal, mais sa syntaxe est totalement différente, donc c'est mieux pour vous de travailler directement sur le serveur. Il est possible d'[activer la même ligne de commande](https://www.eugenetoons.fr/utiliser-le-shell-bash-de-linux-sous-windows-10/) que sous GNU/Linux, mais cela nous éloigne du sujet et rajoute des étapes inutiles.

Si vous débutez, la ligne de commande fait peur, car elle fait l'effet d'une page blanche.
Mais en étant bien guidé, on apprend vite à utiliser cet outil.

Plutôt que de réinventer la roue, nous avons choisi deux introductions à la ligne de commande. Vous pouvez choisir celle qui vous convient le mieux ou lire les deux, comme vous voulez. Vous pouvez faire des essais sur [cette page web](https://webvm.io/) (il faut laisser passer quelques secondes avant de commencer à écrire).

- Sur [doc.ubuntu-fr.org](https://doc.ubuntu-fr.org/tutoriel/console_ligne_de_commande)
- Sur [code-garage.fr](https://code-garage.fr/blog/faire-ses-debuts-sur-la-ligne-de-commande-linux/)

Une petite remarque sur un point qui pourrait vous déstabiliser : sur la plupart des systèmes GNU/Linux, quand un mot de passe est demandé, la ligne de commande n'affiche **rien** quand vous tapez les caractères.


## Louer un serveur

{% note(type="warning", markdown="true") %}
**Question** Un VPS fait-il l'affaire, ou faut-il absolument du bare metal pour le reseau de prod ?
{% end %}

Nous recommandons de louer un serveur pour installer votre noeud.
Ceci vous coûtera une somme modique mensuelle.

Il est également possible d'en installer un chez soi.
Cette seconde option a l'avantage d'être moins coûteuse en monnaie, mais elle nécessitera plus de matériel et d'action de votre part, tout en étant plus sujette à des arrêts (ex. pannes de courant).
Il n'y a aucun problème à installer un serveur chez vous si vous savez ce que vous faites, mais ce n'est pas la voie que nous recommandons pour des débutants.

Ci-dessous une liste non exhaustive, donnée à titre d'exemple, chez lesquels vous pouvez louer un serveur VPS.

(NB - si un fournisseur existe en Ğ1, merci de nous le faire savoir ;-) )

|Fournisseur|Lien|
|:----:|:---:|
|OVH|[Kimsufi (serveurs dédiés)](https://eco.ovhcloud.com/)|
|OVH|[VPS](https://www.ovhcloud.com/fr/vps/)|
|GANDI|[VPS](https://www.gandi.net/fr/cloud/vps)|
|INFOMANIAK|[VPS](https://www.infomaniak.com/fr/hebergement/vps-lite)|
|PULSEHEBERG|[VPS Standard](https://pulseheberg.com/cloud/vps-linux)|
|PULSEHEBERG|[VPS Performance](https://pulseheberg.com/cloud/vps-performance)|

Le serveur doit avoir les caractéristiques minimum suivantes pour le réseau de prod (ça peut être inférieur pour les réseaux de test):

| Usage | RAM | Stockage | Processeur | VPS OK ? | Bande passante |
|:--|:--|:--|:--|:--|:--|
| Miroir | 4Go | ?? TODO ?? | ?? TODO ?? | ?? TODO ?? | ?? TODO ?? |
| Forgeron | 4Go | ?? TODO ?? | ?? TODO ?? | ?? TODO ?? | ?? TODO ?? | 
| Indexeur | -- | ?? TODO ?? | 10 Go | ?? TODO ?? | ?? TODO ?? |
| Datapod | ?? | ?? TODO ?? | 10 Go | ?? TODO ?? | ?? TODO ?? |
| Archive | --- | ?? TODO ?? | 50 Go | ?? TODO ?? | ?? TODO ?? | 


Comme pour la plupart des services payants accessibles par Internet, il faudra vous créer un compte chez le fournisseur.

Lors de la location, votre fournisseur va vous proposer de pré-installer une distribution sur votre serveur. Nous recommandons d'installer **debian** ou **ubuntu** car ce sont deux distributions très répandues pour lesquelles vous trouverez facilement des réponses à vos questions.

Votre fournisseur peut également vous proposer un lieu où héberger votre noeud. Ceci n'a pas d'incidence sur la connexion de votre noeud au réseau Blockchain, mais ça aura une inidence sur la rapidité de réponse de votre noeud. Si vous installez un noeud miroir, nous vous recommandons de choisir une localisation proche de vous.

Les fournisseurs proposent souvent des options supplémentaires (sauvegarde automatisée, stockage supplémentaire, etc.). Ces options ne nous sont pas utilez pour le moment, vous pourrez les activer par la suite si vous en avez besoin.

Après votre paiement, votre fournisseur va vous envoyer les informations pour accéder à votre serveur :
- une adresse IP
- un nom d'utilisateur
- un mot de passe temporaire
- parfois un nom de serveur

Gardez ces informations bien précieusement.

## Se connecter en SSH à son serveur

SSH est un protocole utilisé pour communiquer avec un ordinateur distant, *comme si vous étiez directement sur cet ordinateur*. Une fois connecté en SSH à votre serveur, vous pourrez y faire tout ce que vous voulez.

La connexion se fait de deux façons différentes, suivant si votre poste est sous Windows ou sous Mac / GNU/Linux.

### Sous Mac et GNU/Linux

Il vous faut d'abord ouvrir un terminal.
- Sous GNU/Linux, cette application est souvent directement accessible. Ouvrez le menu, cherchez "term", vous devriez trouver.
- Sous Mac, elle se trouve dans les 'utilitaires'. Vous pouvez aussi la trouver par la recherche d'applications.

Pour mon exemple, j'ai les informations suivantes :
|||
|:---|:---|
|adresse IP|51.75.18.228|
|Nom d'utilisateur|debian|
|Mot de passe|mot_de_passe
|nom de serveur|vps-b540a2aa.vps.ovh.net|

On va ensuite utiliser la commande SSH. La structure est `ssh <utilisateur>@<adresse serveur>`.
En prenant mon exemple, on tape dans le terminal :
```bash
# en utilisant l'adresse IP
ssh debian@51.75.18.228
# en utilisant le nom de serveur
ssh debian@vps-b540a2aa.vps.ovh.net
```
La réponse doit être presque instantanée.
Si vous n'avez rien au bout de quelques secondes, vous pouvez faire `Ctrl + X` et essayer l'autre méthode (IP ou nom du serveur). Si ça ne fonctionne toujours pas, il y a un problème. Relisez bien ce que vous avez tapé, puis contactez le support de votre fournisseur.

SSH va vous demander votre mot de passe.
Une fois le mot de passe entré, votre invite de commande change légèrement. Chez moi par exemple :
```bash
## avant la connexion
matograine@monordi:~$
## connexion
matograine@monordi:~$ ssh debian@vps-b540a2aa.vps.ovh.net
debian@vps-b540a2aa.vps.ovh.net password: 

## Après la connexion
debian@vps-b540a2aa:~$ 
```

Ca y est, vous êtes connecté!

### Windows

Sous windows, nous allons utiliser le logiciel Putty que je vous invite à installer.

Rendez-vous sur [portableApps](https://portableapps.com/apps/internet/putty_portable) et cliquez sur 'Download'.
Vous téléchargez une version *portable* de Putty, c'est-à-dire que vous pouvez la mettre sur clef USB et la lancer depuis n'importe quel poste Windows.
Une fois que vous avez téléchargé le .exe, double-cliquez dessus pour le lancer.
Windows va sans doute vous indiquer un risque, vous pouvez ne pas en tenir compte.

{% note(type="warning", markdown="true") %}
**TODO** Utiliser Putty pour se connecter. Le rédacteur initial de cet article travaille sous LinuxMint, et Putty n'y fonctionne pas. A voir avec un utilisateur de Windose.

NB - ne pas oublier (après sécurisation du serveur) l'aspect 'utilisation de clef SSH' et 'changement du port'.
{% end %}


### Configurer un tunnel SSH

Ue tunnel SSH ou la redirection de port SSH est une méthode de création d’une connexion SSH chiffrée entre un client et une machine serveur à travers laquelle les ports de services peuvent être relayés. Utilisé par exemple, lorsque l'on veut se connecter sur l'interface Polkadotjs de son noeud forgeron, depuis son poste local.

```bash
ssh -L [LOCAL_IP:]LOCAL_PORT:DESTINATION:DESTINATION_PORT [USER@]SSH_SERVER
```

Les options :
- [LOCAL_IP:] LOCAL_PORT – L’adresse IP et le numéro de port de la machine locale. Lorsque LOCAL_IP est omis, le client ssh se lie sur localhost.
- DESTINATION: DESTINATION_PORT – L’adresse IP ou le nom d’hôte et le port de la machine de destination.
- [USER @] SERVER_IP – L’utilisateur SSH distant et l’adresse IP du serveur.


## La sécurité de base du serveur

On trouve plusieurs tutoriels indiquant comment configurer le serveur pour une sécurité de base.
Nous vous invitons à appliquer les conseils de [cet article](https://mindsers.blog/fr/post/comment-securiser-un-serveur-web-vps/).
En particulier, nous vous recommandons d'utiliser `ufw` pour le parefeu (et non `iptables`) car `ufw` est bien plus facile à utiliser.

## Modification de la connexion par SSH

Après avoir sécurisé votre serveur, deux choses auront changé dans la façon de vous connecter par SSH :

- le port SSH a changé
- vous avez peut-être un nouvel utilisateur pour vous connecter par SSH
- vous vous connectez par une clef SSH au lieu d'un mot de passe.

Les paramètres de connexion SSH vont changer :

### GNU/Linux et Mac

La commande devient `ssh -p <port> <utilisateur>@<adresse du noeud>`. En reprenant l'exemple au-dessus :
```bash
ssh -p 49999 debian@vps-b540a2aa.vps.ovh.net
```

Et si vous avez enregistré votre clef SSH dans un endroit particulier, la commande devient `ssh -o "IdentitiesOnly=yes" -i <chemin/vers/la/clef/privee/ssh>   -p <port> <utilisateur>@<adresse du noeud>`. En reprenant l'exemple au-dessus :

```bash
ssh -o "IdentitiesOnly=yes" -i /home/matograine/.ssh/id_rsa_2   -p 49999 debian@vps-b540a2aa.vps.ovh.net
```

Un mot de passe peut vous être demandé pour déverrouiller votre clef SSH.

### Windows avec Putty

{% note(type="warning", markdown="true") %}
🚧 Sous Windows, on peut utiliser Putty pour se connecter. Le rédacteur initial de cet article travaille sous LinuxMint, et Putty n'y fonctionne pas. A voir avec un utilisateur de Windose.
{% end %}

## Mise à jour régulière

Pour rester sécurisé, vous devez mettre votre système à jour régulièrement.
Sous Debian ou Ubuntu, ceci se fait avec la double commande `sudo apt update ; sudo apt upgrade`.
On peut faciliter ceci en lançant un ensemble de commandes à chaque fois que vous vous connectez par SSH.

{% note(type="warning", markdown="true") %}
🚧 documenter comment définir ce jeu de commandes, pour les systèmes UNIX et pour Putty.
{% end %}

