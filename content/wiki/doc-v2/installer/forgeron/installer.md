+++
title="Forgeron Debian"
# date = 2024-06-28
weight = 20
sort_by = "weight"

# [taxonomies]
# authors = ["Matograine",]
+++




# Installer et configurer un noeud forgeron sous Debian

<!-- source : https://git.duniter.org/nodes/rust/duniter-v2s/-/blob/master/docs/user/installation_debian.md?ref_type=heads -->

Ce tutoriel indique les étapes à suivre pour mettre en place un noeud forgeron Duniter V2.

Un noeud forgeron va calculer des blocs. Il est lié en pair-à-pair avec tous les autres noeuds (forgerons et miroirs) mais, contrairement à un noeud miroir, il ne doit pas recevoir de requêtes utilisateurs. La raison est simple : l'API permettant d'exécuter des requêtes sur un noeud permet de modifier ses clefs de calcul. Il suffit de mettre en place de fausses clefs de calcul pour que le noeud cesse de produire des blocs.

Mettre en place un noeud forgeron implique de pouvoir intervenir rapidement si un problème est détecté (par exemple pour relancer son noeud). Cela implique également que le serveur en place soit continuellement connecté à Internet.

Ce document vous indique les différentes étapes pour installer un noeud forgeron sous Debian.
Certaines étapes sont documentées sur des pages annexes, en particulier les prérequis et l'installation du logiciel Duniter V2.
Nous vous conseillons de prendre votre temps et de bien faire chaque étape.

[TOC]

## Prérequis

Ce tutoriel suppose que :
- vous savez utiliser la [ligne de commande](@/wiki/doc-v2/installer/prerequis.md#utiliser-la-ligne-de-commande)
- vous avez [un serveur installé sous Debian](@/wiki/doc-v2/installer/prerequis.md#louer-un-serveur)
- vous pouvez vous y connecter en [ssh](@/wiki/doc-v2/installer/prerequis.md#se-connecter-en-SSH-a-son-serveur)
- vous avez fait une [sécurisation de base du serveur](@/wiki/doc-v2/installer/prerequis.md#la-securite-de-base-du-serveur)

Les paquets que nous allons télécharger sont pour des serveurs ayant une architecture amd64. Ceci fonctionnera pour les serveurs loués, et pour un serveur à la maison s'il a une architecture amd64. Mais pour des serveurs ARM (typiquement un raspberry pi), ces fichiers ne fonctionneront pas, il vous faudra compiler Duniter.

## Installation et configuration

L'installation se déroule de la même façon que pour un noeud miroir, avec des changements lors de la configuration : 

- la constante de configuration `DUNITER_RPC_CORS` doit être égale à 'http://localhost:*,http://127.0.0.1:*', car nous voulons que le noeud ne soit accessible qu'en local sur l'API RPC.

### Avec Debian

Sous **Debian**, Vous pouvez donc suivre le même mode opératoire que pour [installer et configurer un noeud miroir](@/wiki/doc-v2/installer/miroir/debian.md), à cette unique différence près. Arrétez-vous à 'Lancer le service Duniter'.

#### Lancer le service Duniter

Duniter fournit une commande pour être lancé comme noeud miroir :
```bash
## démarrer le service
sudo systemctl start duniter-smith.service
## activer le service au démarrage
sudo systemctl enable duniter-smith.service
```

### Avec Docker

Avec **Docker**, vous devrez suivre [cette documentation](@/wiki/doc-v2/installer/forgeron/docker.md) spécifique aux forgerons (même si le mode opératoire est très proche d'un noeud miroir).

## Configuration du parefeu

Suivez la documentation pour la [mise en place du parefeu]([documentation](@/wiki/doc-v2/installer/memo/configurer-parefeu.md)) en ouvrant les ports :

- SSH (22 par défaut, mais vous l'avez peut-être changé)
- 30333 (ou le port P2P que vous avez configuré)

Les autres ports doivent être fermés si vous n'avez que le noeud forgeron sur votre serveur.

## Génération des clefs de noeud

Il faut ensuite générer les clefs du noeud, la méthode change si vous êtes sous Docker ou Debian :

```bash
### Debian ###
# <BASE_PATH> doit être remplacé par la valeur de BASE_PATH dans /etc/duniter/env_file
# <DUNITER_CHAIN_NAME> doit être remplacé par la valeur de DUNITER_CHAIN_NAME dans /etc/duniter/env_file
duniter2 key generate-node-key --base-path <BASE_PATH> --chain <DUNITER_CHAIN_NAME>

### Docker ###
# ici node_duniter-smith_1 est le NAME du conteneur, que vous pouvez obtenir avec `docker container ls`
# le BASE_PATH est celui configuré dans le dockerfiles, 'duniter-data:\var\lib\duniter'
docker exec -it node_duniter-smith_1 duniter key generate-node-key  --base-path /var/lib/duniter --chain <DUNITER_CHAIN_NAME>
```
Si vous rencontrez l'erreur `Skip generation, a key already exists` c'est que tout va bien, une clef existe déjà.


## Activer le calcul de blocs sur son noeud

### Génération des clefs du noeud (bis)

On suppose que vous avez [installé Ğcli](@/wiki/doc-v2/installer/gcli/_index.md) sur votre serveur.
vérifions d'abord que vous pouvez utiliser gcli sur votre serveur :
```bash
## On passe le paramètre -u ws://localhost:9944 pour accéder au noeud local. Modifiez le port si besoin.
$ gcli -u ws://localhost:9944 blockchain current-block
> on ws://localhost:9944
> finalized block	3477672
> current block	3477674
```

On va configurer gcli pour un usage sur le noeud forgeron.
On va créer un fichier de configuration local :
```bash
## où se trouve le fichier de config ?
$ gcli config where
> /home/toto/.config/gcli/default-config.toml
## Copier le fichier de config
$ cp /home/toto/.config/gcli/default-config.toml /home/toto/.config/gcli/default-config.toml.OLD
## sauvegarder la nouvelle configuration
## il est essentiel d'avoir son paramètre -u en localhost, sauf configurations très exotiques. Vous pouvez modifier le port vers le port RPC de votre noeud si ce n'est pas 9944.
## l'indexeur peu être choisi à votre convenance. Mettes bien le chemin d'API /v1/graphql
## l'adresse est votre adresse membre
$ gcli -u ws://localhost:9944 -i https://squid.gdev.coinduf.eu/v1/graphql -a <VOTRE_ADRESSE_G1>  config save
```

Puis on modifie les clefs du noeud :
```bash
$ gcli smith update-keys
> Mnemonic: 
> transaction submitted to the network, waiting 6 seconds...
```

### Passer en ligne

Il faut déclarer que votre noeud est prêt à calculer avec la commande `go-online` :

```bash
$ gcli smith go-online
> Mnemonic: 
> transaction submitted to the network, waiting 6 seconds...
> smith went online MemberGoOnline { member: 1047 }
```

Ceci sera pris en compte au début de la session suivante (les sessions durent une heure), donc votre noeud ne va pas calculer tout de suite.
Si vous devez déconnecter votre noeud, anticipez la chose et faites `gcli smith go-offline`. Votre noeud sera considéré offline à partir de la fin de la prochaine session, soit au plus deux heures plus tard.


