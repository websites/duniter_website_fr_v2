+++
title="Toile de confiance Forgeron"
# date = 2024-06-28
weight = 20
sort_by = "weight"

# [taxonomies]
# authors = ["Matograine",]
+++



# Entrer dans la toile de confiance forgerons

La toile de confiance forgerons est un groupe de membres de Ğ1 qui ont les compétences informatiques suffisantes pour faire tourner un noeud calculant de façon sécurisée. Contrairement à la toile de confiance membre, la certification forgeron n'implique pas que la personne qui vous certifie vous connaît bien. La personne qui vous certifie doit évaluer que vous avez mis des mesures de sécurité en place et être en mesure de vous joindre rapidement. Il est donc tout à fait légitime de demander par internet à d'autres forgerons s'ils peuvent vous y faire entrer.

En tant que membre forgeron, vous pourrez faire entrer d'autres personnes dans la toile de confiance forgeron. Mais ceci n'implique pas que vous devez répondre aux demandes si vous ne vous sentez pas les capacités à évaluer ce qu'ont fait les candidates.

## Licence forgerons

La charte de la toile de confiance forgeron peut être trouvée [ici](https://git.duniter.org/1000i100/g1_charter/-/blob/master/smith_charter/g1_smith_charter_fr.adoc). Voici les points que vos certifiants devront valider, en résumé :

- [ ] Vous joindre rapidement et pas plusieurs moyens
- [ ] Vérifier avec votre configuration (ceci implique que vous lui envoyiez le fichiers de configuration de votre noeud)
- [ ] Vérifier que vous avez un noeud à jour et synchronisé (votre noeud doit apparaître dans l'écran de [télémétrie](https://polkadot.js.org/apps/?rpc=wss%3A%2F%2Fgdev.coinduf.eu%2Fws#/explorer) Si vous n'avez pas activé la télémétrie, une autre option doit être trouvée.)
{% note(type="warning", markdown="true") %}
Trouver une option alternative à la télémétrie
{% end %}
- [ ] Evaluer que vos pratiques de sécurité sont suffisantes. Ceci peut couvrir la configuration du serveur sur lequel se trouve le noeud s'il héberge d'autres services, ou les modalités de sauvegarde et de stockage de vos clefs privées.

Lorsque vous certifiez, reportez-vous à la charte et non au présent document.

## Demander à entrer dans la toile forgeron

Pour entrer dans la toile forgerons, il vous faut donc :

1. faire la demande auprès d'au moins 5 forgerons
1. répondre à leurs questions
1. recevoir et confirmer votre invitation
2. recevoir les certifications

Les points 1 et 2 se font hors blockchain. Contactez des forgerons que vous connaissez, demandez sur les forums, etc. Votre premier certifiant va vous envoyer une invitation.

## Recevoir et confirmer votre invitation

Nous allons faire ceci avec `gcli`. Si vous ne l'avez pas installé, [faites-le maintenant](@/wiki/doc-v2/installer/gcli/_index.md).

On va accepter l'invitation :
```bash
## <NETWORK> : gtest, gdev ou g1
## <MEMBER_ADDRESS> : votre adresse de compte membre
gcli -n <NETWORK> -a <MEMBER_ADDRESS> smith accept
> Enter password to unlock account XXX...
> Password: 
> transaction submitted to the network, waiting 6 seconds...
```

NB: comme vous vous authentifiez pour votre compte membre, vous devez utiliser votre mnémonique **suivi du [chemin de dérivation](@/wiki/doc-v2/concept/compte.md#derivation)** `//2`

Ensuite, prévenez les certifiants qui doivent *tous* vous certifier (y compris celui qui vous a invité). Une fois vos <span class="blockchain-param">5</span> certifications reçues, vous êtes membre de la toile forgeron : votre noeud a le droit de forger des blocs.

