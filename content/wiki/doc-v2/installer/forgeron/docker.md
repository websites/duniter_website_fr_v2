+++
title="Installation noeud forgeron avec Docker"
# date = 2024-06-28
weight = 20
sort_by = "weight"

# [taxonomies]
# authors = ["Matograine",]
+++




# Installer et configurer un noeud forgeron avec Docker 

Docker est une plateforme permettant de lancer certaines applications dans des conteneurs, ce qui permet une exécution plus sécurisée. [pour en savoir plus](@/wiki/doc-v2/installer/debuter-avec-docker.md)

Ce document vous indique les différentes étapes pour installer un noeud miroir avec Docker.
Certaines étapes sont documentées sur des pages annexes : les prérequis et la mise en place du reverse-proxy.
Nous vous conseillons de prendre votre temps et de bien faire chaque étape.

[TOC]

## Prérequis

Ce tutoriel suppose que :
- vous savez utiliser la [ligne de commande](@/wiki/doc-v2/installer/prerequis.md#utiliser-la-ligne-de-commande)
- vous avez [un serveur installé sous Debian](@/wiki/doc-v2/installer/prerequis.md#louer-un-serveur)
- vous pouvez vous y connecter en [ssh](@/wiki/doc-v2/installer/prerequis.md#se-connecter-en-SSH-a-son-serveur)
- vous avez fait une [sécurisation de base du serveur](@/wiki/doc-v2/installer/prerequis.md#la-securite-de-base-du-serveur)

Connectez-vous à votre serveur en SSH.

## Installation de Docker

Nous allons commencer par installer Docker. Cette partie est basée sur [la documentation Docker](https://docs.docker.com/engine/install/debian/).

Mettre à jour le système :

```bash
sudo apt-get update ; sudo apt-get upgrade
```

Supprimer les paquets Docker éventuellement déjà installés :

```bash
for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do sudo apt-get purge $pkg; done
```

Configurer les dépôts Apt de docker ([source](https://docs.docker.com/engine/install/debian/#install-using-the-repository)) :

```bash
# Ajouter la clef PGP officielle de Docker:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Ajouter le dépôt Docker à notre liste de dépôts :
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
```

Installer les paquets :

```bash
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

Vérifier l'installation en lançant l'image `hello-world` :

```bash
sudo docker run hello-world
```

## Création du dockerfile

Les images Docker utilisent une série de paramètres au lancement, qu'on configure dans un fichier *docker-compose*. Ce fichier doit se trouver dans un dossier spécifique.

Créer le dossier et le docker-compose :
```bash
cd ~
mkdir duniter
cd duniter
nano docker-compose.yml
```

La commande `nano` a ouvert un éditeur de texte. Copiez-y la configuration suivante :

```
# Duniter smith node
services:
  duniter-smith:
    image: duniter/duniter-v2s-gdev-800:latest         # <--- image Docker
    restart: unless-stopped
    ports:
      # prometheus endpoint for monitoring
      - 127.0.0.1:9615:9615
      # private rpc endpoint (do not expose publicly)
      - 127.0.0.1:9944:9944
      # public p2p endpoint
      - 30333:30333                             # <--- port p2p public
    volumes:
      - duniter-data:/var/lib/duniter/
    environment:
      - DUNITER_CHAIN_NAME=gdev                               # <--- réseau où se connecter
      - DUNITER_NODE_NAME=YOUR_NAME-smith                     # <--- le nom de votre noeud
      - DUNITER_VALIDATOR=true                                # <--- activer le noeud validateur
      - DUNITER_PRUNING_PROFILE=light                         # <--- conserver 256 blocs d'historique
      - DUNITER_PUBLIC_ADDR=/dns/gdev.EXAMPLE.ORG/tcp/30333   # <--- endpoint p2p public
      - DUNITER_LISTEN_ADDR=/ip4/0.0.0.0/tcp/30333            # <--- adresse d'écoute p2p
volumes:
  duniter-data:
```

Remplacez `YOUR_NAME` par le nom que vous souhaitez donner au noeud, `gdev` par le réseau qu vous voulez rejoindre et `gdev.EXAMPLE.ORG` par le nom de domaine que vous utilisez..

Cette configuration est minimale. Elle implique que le 30333 du serveur soit ouvert. Si vous souhaitez l'adapter (par exemple en modifiant des ports), reportez-vous aux [variables d'environnement disponibles](https://git.duniter.org/nodes/rust/duniter-v2s/-/blob/master/docker/README.md)


## Lancement du service

Pour lancer le service, placez-vous dans le dossier qui contient le `docker-compose.yml` et lancez la commande suivante :
```bash
docker compose up -d
```

Pour voir les logs en temps réel :
```bash
docker compose logs -f
```

Pour avoir des informations sur le conteneur :
```bash
docker container ls
CONTAINER ID   IMAGE                             COMMAND               CREATED          STATUS         PORTS                                                                                                                                 NAMES
86bf09f0bd24   duniter/duniter-v2s-gdev:latest   "docker-entrypoint"   22 seconds ago   Up 6 seconds   0.0.0.0:9615->9615/tcp, :::9616->9615/tcp, 0.0.0.0:9944->9944/tcp, :::9944->9944/tcp, 0.0.0.0:30333->30333/tcp, :::30333->30333/tcp   node_duniter-smith_1
``` 

## Vérifier le bon fonctionnement du noeud

Si vous ne l'avez pas déjà fait, [installez Ğcli](@/wiki/doc-v2/installer/gcli/_index.md) sur votre serveur.

Puis regardez si le noeud est accessible en local :

```bash
gcli -u ws://localhost:9944 blockchain current-block
```
on doit obtenir :
```
on ws://localhost:9944
finalized block	2208389
current block	2295714
```
