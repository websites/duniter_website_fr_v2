+++
title="Installer un noeud forgeron"
# date = 2024-06-28
weight = 40
sort_by = "weight"

# [taxonomies]
# authors = ["JossRendall",]

[extra.translations]
en = "wiki/duniter-v2/run-smith/"
+++



## Monter un Noeud Forgeron

Dans le [vocabulaire de Duniter](@/wiki/doc-v2/glossaire.md), le terme "smith" est attribué à une personne autorisée à ajouter des blocs dans la blockchain. Contrairement à Duniter v1, le rôle de forgeron est principalement technique, la gouvernance étant effectuée *sur la chaîne*. L'installation et le maintien d'un noeud sont relativement faciles à mettre en place pour qui est à l'aise en informatique, mais l'appartenance aux forgerons implique de veiller aux évolutions des recommandations de sécurité et de répondre aux sollicitations d'apprentis forgerons.

Un forgeron doit :
- suivre de bonnes pratiques de sécurité pour ses clés et son nœud
- être capable de réagir rapidement en cas de défaillance (ou de mise hors ligne)
- contribuer au réseau de confiance des smiths en invitant d'autres smiths à se joindre à lui.

Voici les étapes à suivre pour devenir "smith":
1. [Installer un nœud forgeron](@/wiki/doc-v2/installer/forgeron/installer.md)
4. Devenir un [membre de la Toile de Confiance Forgeron](@/wiki/doc-v2/installer/forgeron/toile-de-confiance.md), ce qui implique de maintenir son noeud dans la durée.
<!--7. en option : [créer un Oracle de Distance](@/wiki/doc-v2/tmp.md) (qui calcul et vérifie les règles de distance de la toile de confiance) -->
