+++
title = "Glossaire"
# date = 2024-06-28
weight = 99

# [taxonomies]
# authors = ["JossRendall",]

[extra.translations]
en = "/wiki/duniter-v2/glossary/"
+++

# Glossaire

## Vocabulaire pour Duniter

**forgeron** : Membre qui à le pouvoir de "forger" des blocs.

**forger des blocs** : Généralement, les blockchains utilisent le terme "miner des blocs" car cette action est récompensée par la génération de monnaie (métaphore avec une mine d'or). Mais comme la création de monnaie avec Duniter est uniquement faite avec le Dividende Universel, nous avons choisi une autre métaphore pour désigner l'écriture d'un block.

**compte** : Trousseau cryptographique, composé d'une clef privée et d'une clef publique, permettant de recevoir et envoyer des transactions. Par extension, on utilise le mot *compte* pour désigner l'*adresse d'un compte*, c'est-à-dire une chaîne de caractères représentant la clef publique.

**identité** : Une identité fait le lien entre un compte et un individu, grâce à la *toile de confiance*. Une identité est liée à un seul compte à in instant T, mais peut être liée successivement à plusieurs comptes. Une identité est liée à un seul individu. Une identité peut recevoir et émettre des certifications pour faire entrer de nouveaux individus dans la toile de confiance.

**compte membre** : Compté lié à une identité. Par abus de language, on parle de "certifier un compte membre" quand on certifie une identité.

## Vocabulaire pour Substrate

**autorité** : Une autorité est habilitée à devenir un validateur, puis à créer des blocs.

**validateur** : Un nœud de validation est un nœud configuré pour ajouter des blocs à la blockchain. Par extension, les validateurs sont les personnes qui détiennent les clés de ces nœuds.


