+++
aliases = ["fr/wiki",]
# date = 2024-06-26
weight = 2
title = "Wiki"
insert_anchor_links = "right"

# [taxonomies]
# authors = ["HugoTrentesaux", "cgeek", "matiou",]
# tags = ["blockchain", "TRM", "yunohost",]

[extra.translations]
en = "wiki/"
+++

# Wiki

Bienvenue sur le wiki de Duniter ! Vous trouverez ici toutes les informations techniques et explications détaillées à propos du projet Duniter. Pour trouver rapidement une ressource, utilisez la **barre de navigation** en haut de page 👆. Sinon, laissez-vous guider par le texte.

## Documentation de Duniter

Duniter est le logiciel qui fait fonctionner la monnaie Ğ1. Vous trouverez la documentation développeur et utilisateur dans le dossier [Documentation Duniter](@/wiki/doc/_index.md) ainsi que la documentation d'une partie de l'écosystème.

<div class="center-content"><a class="w3-button w3-blue w3-round" href="/wiki/doc/">Doc v1</a></div>

## Documentation de Duniter-v2s

Duniter est actuellement réécrit avec le framework Substrate. C'est une évolution du logiciel originel qui fait fonctionner la monnaie Ğ1. Vous trouverez la documentation développeur et utilisateur dans le dossier [Documentation Duniter-v2s](@/wiki/doc-v2/_index.md) ainsi que la documentation d'une partie de l'écosystème.

<div class="center-content"><a class="w3-button w3-orange w3-round" href="/wiki/doc-v2/">Doc v2</a></div>

## Monnaie Libre

La monnaie libre est un concept introduit en 2010 par la théorie relative de la monnaie (TRM). Elle admet une définition précise que vous pourrez explorer dans la section [Monnaie Libre](@/wiki/monnaie-libre/_index.md) de ce forum. De plus en plus de personnes utilisent le mot "monnaie libre" pour désigner les monnaies locales ou les cryptomonnaies. Nous ne traitons pas ces sujets ici, et nous concentrons sur la définition précise donnée par la TRM.

<div class="center-content"><a class="w3-button w3-indigo w3-round" href="/wiki/monnaie-libre/">Monnaie libre</a></div>

## Monnaie Ğ1

La monnaie Ğ1 (G-une, ou "june") est la première monnaie libre au sens donné par la TRM. C'est une expérimentation grandeur nature qui compte plus de 6000 membres créateurs. Vous trouverez toutes les informations techniques sur ğ1 dans la section [Monnaie Ğ1](@/wiki/g1/_index.md). Pour les informations pratiques relatives à son utilisation, veuillez plutôt consulter le site [https://monnaie-libre.fr/](https://monnaie-libre.fr/).

<div class="center-content"><a class="w3-button w3-green w3-round" href="/wiki/g1/">Ğ1</a></div>

## Toile de confiance

La toile de confiance de la monnaie ğ1, rendue possible par le logiciel Duniter, est une innovation intéressante permettant de répondre au besoin d'un système d'identification pair-à-pair. Elle constitue un élément central du projet Duniter/Ğ1 mais est indépendante de la notion de monnaie libre. C'est pour ces raisons que vous trouverez les informations qui s'y rapportent dans une section dédiée : [Toile de confiance](@/wiki/toile-de-confiance/_index.md). 

<div class="center-content"><a class="w3-button w3-lime w3-round" href="/wiki/toile-de-confiance/">Toile de confiance</a></div>

## Guide de contribution

Le projet vous plaît et vous souhaitez y consacrer du temps ou y apporter votre savoir-faire ? Un [guide de contribution](@/wiki/contribuer/_index.md) est disponible pour vous aiguiller dans ce vaste projet !

## À propos

Pour les informations relatives à ce site, voir la section [à propos](@/wiki/about/_index.md).
