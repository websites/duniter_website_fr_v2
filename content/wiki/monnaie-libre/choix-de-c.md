+++
title = "Choix de c"

[extra]
katex = true
+++


#### Conséquence du choix de c
Si l'on choisit $c$ trop grand, la monnaie va se "dévaluer" trop vite, ce qui nuit à sa fonction de réserve de valeur. Si, au contraire, $c$ est choisi trop petit, la monnaie manquera dans l'économie, ce qui accentuera les inégalités. Pour savoir si une valeur est "trop forte" ou "trop faible", la TRM se fonde sur l'espérance de vie $\text{ev}$ de l'individu et donne deux valeurs extrêmes pour $c$, cf [l'article détaillé](https://www.creationmonetaire.info/2019/08/c-fvx.html) sur le sujet. L'idée a été [reprise ailleurs](http://monnaie.ploc.be/trm-en-detail/), pour arriver à des conclusions similaires.

La valeur de $c$ est plus facile à comprendre si l'on parle en taux de création monétaire annuel $\tau = e^{c\times1\text{ an}} - 1$. Les valeurs limites proposées par la TRM sont de 2.2% (borne inférieure) et 14.3% (borne supérieure).