+++
title = "Ğ1"
weight = 3

[extra.translations]
en = "wiki/g1/"
+++

# Monnaie Ğ1

La monnaie Ğ1 (prononcée "*june*") est la première [monnaie libre](@/wiki/monnaie-libre/_index.md) au sens de la TRM. Elle a été créée à l'aide du logiciel Duniter.
Tout le monde est libre d'utiliser la ğ1, il suffit pour cela de créer un compte sur un logiciel client comme Césium ou Ğecko et de commencer à échanger. Pour créer des ğ1, il faut rejoindre la [toile de confiance](@/wiki/toile-de-confiance/_index.md) qui est le meilleur moyen que nous avons trouvé pour garantir que tout le monde crée une part égale de monnaie et pas plus que les autres. Pour cela il faut lire la [Licence Ğ1](@/wiki/g1/licence-g1.md) et s'engager à la respecter auprès de cinq membres créateurs de june au minimum.

Vous trouverez ici des informations techniques sur l'utilisation de la Ğ1. Pour les aspects monétaires et sociétaux, référez-vous au site [monnaie-libre.fr](https://monnaie-libre.fr/).

{% note(type="warning") %}
TODO faire une bonne documentation utilisateur sur le fonctionnement des outils.
(Cesium / Ğecko / Wotwizard et cie)
{% end %}


## Licence Ğ1

La licence définit les règles pour rejoindre la toile de confiance de la Ğ1.

* [Licence Ğ1](@/wiki/g1/licence-g1.md) (devenir membre Ğ1 + règles de la monnaie + règles de la Toile de Confiance)

## Supports de communication

Dépôt media regroupant les logos, banières et autres outils de communication visuels :

* [Communication G1](https://git.duniter.org/communication/G1)
* [Logos Logiciels](https://git.duniter.org/communication/logos)