+++
title = "Duniter v2 alpha 🌀"
description = "Cela fait un moment que nous avons commencé à travailler sur l'implémentation Duniter v2 dans le framework Substrate. Nous annonçons ici une nouvelle étape."

[extra]
thumbnail = "/img/duniterv2-alpha.png"

[extra.translations]
en = "blog/duniter-v2-alpha/"

[taxonomies]
authors = ["bot",]
category = ["Moteur blockchain",]
# tags = []
+++

<br>
<br>

{% note(type="info",markdown=true) %}
Cet article est une **traduction automatique** depuis sa version en anglais. Il contient certaines opinions de l'auteur qui pourraient ne pas être partagées par d'autres contributeurs de Duniter. (marquées par '🞰' dans le texte ci-dessous)
{% end %}

# Duniter v2 alpha 🌀

Cela fait un moment que nous avons annoncé la version 2 de Duniter dans [cet article de blog](@/blog/2022-01-29-duniter-substrate.md). Depuis ce temps, beaucoup de travail a été accompli, non seulement sur [Duniter v2 lui-même](@/logiciels/duniter-v2.md), mais sur tout l'écosystème v2, y compris l'[indexeur Duniter Squid](@/logiciels/duniter-squid.md) et les clients de version 2 comme [Cesium v2](@/logiciels/cesium-v2.md) et [Ğecko](@/logiciels/gecko.md). Il est maintenant temps d'annoncer la version alpha de Duniter v2, prêt pour les tests avec le réseau `gdev`.

[TOC]

## Une blockchain toujours autonome 💫

Duniter v1 est une *solo chain* gérée par les membres de la toile de confiance Ğ1. Bien que nous utilisions Substrate, le <abbr title="Software Development Kit">SDK</abbr> de Polkadot, nous ne voulons pas être une parachain de {{si(id="polkadot")}}Polkadot ou Kusama pour des raisons à la fois techniques et politiques. Du point de vue technique, la toile de confiance nécessite de stocker beaucoup de données en blockchain ce qui est incompatible avec certaines limitations des parachains. Du point de vue politique, nous voulons que les outils restent entre les mains de leurs utilisateurs pour toujours et ne pouvons pas déléguer le consensus à une "chaîne relais" qui coûte de la "monnaie non-libre", même si ce coût est offert dans le cadre du programme "[common good parachain](https://polkadot.network/blog/proposal-for-common-good-parachains/)". C'est pourquoi nous invitons les membres de la Ğ1 qui tiennent à son indépendance à rejoindre le réseau de test `gdev`.

## Rejoindre le réseau de test ☄

Nous avons déjà lancé plusieurs réseaux de test par le passé pour nous familiariser avec la technologie. Maintenant que le logiciel se stabilise, nous sommes prêts à accueillir plus de personnes sur notre réseau de test `gdev`. Vous pouvez rejoindre en tant que nœud miroir si vous souhaitez simplement découvrir, et annoncer un *endpoint* RPC public si vous voulez participer à la décentralisation. Si vous vous sentez confiant et souhaitez participer au consensus, vous pouvez même exécuter un nœud smith. La [documentation est disponible dans la section wiki en anglais](https://duniter.org/wiki/duniter-v2/).

Le réseau de test est un moyen de se familiariser avec le système d'identité Duniter. Comme c'est un réseau de test, vous pouvez obtenir une certification sans rencontres <abbr title="In Real Life">IRL</abbr> simplement en fournissant une clé publique dans la [section Ğdev](https://forum.duniter.org/c/currencies/gdev/63)du forum. Les clients en version bêta sont configurés pour se connecter à ce réseau de test par défaut.

## La blockchain en tant que bien commun 🌲

Nous avons déjà mentionné l'approche "logiciel en tant que bien commun" du projet Duniter dans [l'article précédent](@/blog/2022-01-29-duniter-substrate.md#la-blockchain-comme-ressource-commune-evergreen-tree) :

> Bien commun : *"une ressource gérée collectivement par une communauté"*

Nous avons approfondi ce sujet avec la communauté Ğ1, en particulier pour résoudre le problème des frais, mais aussi sur les aspects de gouvernance. Plus de détails ci-dessous.

### Une solution au problème des frais 💸

La blockchain est une ressource partagée et ouverte, pas comme un système multi-utilisateurs qui limite les connexions SSH à un ensemble de clés préconfigurées connues, mais comme une machine virtuelle qui exécute le même code de manière distribuée. Elle dispose d'un système de permission et doit prendre en compte le coût d'exécution si elle ne veut pas être attaquée par une attaque par déni de service. Comme c'est un système distribué, il n'y a pas moyen de simplement se fier aux limitations par IP et d'autres mesures doivent être mises en œuvre.

Les utilisateurs sont identifiés par une clé publique cryptographique avec un nombre "infini" de possibilités dans le sens où il est extrêmement improbable que la même clé soit choisie deux fois avec une génération aléatoire correcte (IPv4 est de 32 bits, IPv6 est de 128 bits, nous utilisons des clés publiques ed25519 de 256 bits). Cela est nécessaire pour la sécurité des clés mais empêche l'application d'un quota par clé. Parce que nous avons une toile de confiance, nous pourrions limiter l'utilisation de la blockchain à un ensemble restreint de clés, mais cela empêcherait les comptes anonymes (non liés à une partie de la TdC) d'utiliser la blockchain. Toutes les blockchains publiques dont nous avons entendu parler ont adopté la même solution : les frais. Toute opération sur la blockchain coûte de l'argent à l'auteur, ce qui limite la quantité de ressources qu'il peut utiliser. De plus, les validateurs peuvent prioriser les transactions en fonction de leur propre intérêt grâce à un mécanisme de pourboire.

Cette solution a plusieurs problèmes incompatibles avec notre vision du logiciel en tant que bien commun et d'une monnaie égalitaire :
- l'utilisation de la ressource commune n'est pas *vraiment* gratuite, elle vient avec un coût
- ceux qui possèdent plus ont plus de droits d'utiliser la blockchain (ils paient moins de frais en proportion de leur richesse totale)
- ceux qui possèdent plus sont privilégiés par rapport aux autres (ils peuvent payer un pourboire plus élevé au validateur pour être priorisés)

Dans certaines situations comme {{si(id="ethereum")}}Ethereum, le coût ("gaz") est devenu si prohibitif que de nombreux utilisateurs ont quitté la plateforme. Dans certaines situations comme {{si(id="solana")}}Solana, le coût de base est si bas que les <a href="https://fr.wikipedia.org/wiki/Attaque_par_d%C3%A9ni_de_service">attaques <abbr title="Distributed Denial of service">DDos</abbr></a> ne sont pas assez chères et se produisent fréquemment, déclenchant des frais de priorisation.

Dans notre recherche pour combiner le meilleur des deux mondes, nous avons trouvé une solution qui devrait permettre une utilisation totalement gratuite de la blockchain la plupart du temps, et pendant les attaques une utilisation gratuite limitée par un quota pour les humains identifiés par la toile de confiance. Cette solution est basée sur le framework de benchmark de substrate. Comme nous connaissons assez précisément le coût du pire scénario en termes de taille de stockage et de temps d'exécution, ainsi que la capacité maximale de la machine de référence, nous pouvons définir un seuil d'activité au-dessous duquel chaque action est gratuite. Lorsque ce seuil est dépassé — très probablement à cause d'une attaque — les frais cessent d'être nuls et l'attaque s'épuise par manque de moyens. Pendant ces attaques, les comptes non anonymes (= liés à la toile de confiance) bénéficient toujours de frais nuls (sous quota).

Nous pensons que cette solution encouragera les personnes lassées des frais bancaires à essayer notre blockchain et permettra à des applications inattendues, non pertinentes en présence de frais, d'émerger. Par exemple une comptabilité interne transparente basée sur un système de comptes publics au sein des organisations.

### Gouvernance *onchain* pour les mises-à-jour du code 🆙

Les systèmes avancés de gouvernance sur blockchain ("<a href="https://en.wikipedia.org/wiki/Decentralized_autonomous_organization"><abbr title="Decentralized Autonomous Organization">DAO</abbr></a>") pour les mises-à-jour du code comme ceux utilisés par Kusama sont intéressants, mais :
- cela masque le fait que le développement logiciel est fortement guidé par les financements
- ce n'est pas bien adapté à la communauté Ğ1, de petite taille, fortement soudée, et non technique

Pour le moment, nous voulons simplement rendre le fonctionnement v1 plus transparent. D'abord, les développeurs ont une idée, ils l'implémentent bénévolement, en discutent avec les autres développeurs, et la soumettent aux forgerons. Ces derniers ne sont pas nécessairement des techniciens, mais ils font confiance aux développeurs et installent la mise-à-jour. En v2, nous devions impérativement augmenter la sécurité du groupe forgeron par une toile de confiance forgeron (plus d'informations à ce sujet plus tard) et nous devions nous doter d'une capacité de mise-à-jour plus réactive, d'où, un comité technique mieux informé. Nous avons d'abord pensé que ce serait un problème de centralisation pour la communauté, mais nous avons constaté que : 1️⃣ c'est une particularité des systèmes de consensus sans fork qu'un système de vote plus sophistiqué peut aider à passer à l'échelle mais pas résoudre (🞰), 2️⃣ ce n'est pas ce que la communauté attend en matière de développement logiciel, d'où la section suivante (🞰).

### Un écosystème logiciel construit avec la communauté (🞰) 🤝

Ce que la communauté attend *vraiment* n'est pas une perfection théorique dans le processus de prise de décision, mais des évolutions de l'application finale qu'ils auront en main. En d'autres termes, la manière dont les interfaces utilisateurs sont conçues est plus importante pour l'utilisateur moyen que les fondations techniques sur lequel elles reposent. Il est plus important que leurs demandes de fonctionnalités soient implémentées dans un délai raisonnable que le processus de cette mise en œuvre. Nous concentrerons donc d'abord nos efforts sur l'établissement et le maintien d'un lien fluide entre la volonté de la communauté et l'implémentation, et mettrons en place seulement ensuite des mécanismes d'auto-gouvernance plus "bottom-up" que "top-down". Cela signifie que nous expérimenterons probablement d'abord avec des systèmes d'information et de vote *offchain* avant de les implémenter *onchain*.
