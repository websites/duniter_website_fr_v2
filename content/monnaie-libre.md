+++
title = "Monnaie libre"
template = "custom/2-monnaie-libre.html"
weight = 1
description = "Une monnaie libre est une monnaie qui respecte les principes de symétrie spatiale et temporelle. La création monétaire libre n'introduit pas d'inégalités entre les individus."

[taxonomies]
tags = ["Monnaie Libre", "TRM", "monnaie égalitaire"]
+++