+++
title = "VanityGen"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "fa-book"
repo = "https://github.com/jytou/vanitygen"
website = ""

+++

<a href="https://github.com/jytou/vanitygen">VanityGen</a> permet de créer une clef publique contenant un certain schéma.