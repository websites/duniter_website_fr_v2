+++
title = "Remuniter"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "fa-database"
repo = ""
website = "https://remuniter.cgeek.fr/"

+++

<a href="https://remuniter.cgeek.fr/">Remuniter</a> est une caisse commune pour <a href="/wiki/duniter/forger-des-blocs/">rémunérer automatiquement</a> l'ajout d'un bloc dans la blockchain.