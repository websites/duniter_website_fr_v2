+++
title = "Silkaj"

[taxonomies]
authors = ["moul", "vit"]
language = ["python"]
framework = []

[extra]
logo = "/img/silkaj.svg"
repo = "https://git.duniter.org/clients/python/silkaj/"
website = "https://silkaj.duniter.org/"
+++

Client en ligne de commande développé en <strong>Python</strong>, <a href="https://silkaj.duniter.org/">Silkaj</a> permet notamment d'automatiser certaines tâches complexes.