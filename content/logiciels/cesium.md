+++
title = "Cesium"

[taxonomies]
authors = ["kimamila", "matograine"]
language = ["javascript",]
framework = ["angularjs", "ionic"]

[extra]
logo = "/img/cesium.svg"
repo = "https://git.duniter.org/clients/cesium-grp/cesium/"
website = "https://cesium.app/fr/"
+++

Fondé sur les frameworks <strong>AngularJS</strong> et <strong>Ionic</strong>, <a href="https://cesium.app/">Cesium</a> est un client web également disponible sur smartphone.