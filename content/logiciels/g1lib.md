+++
title = "Ğ1lib"

[taxonomies]
authors = ["1000i100"]
language = ["javascript", "typescript"]
framework = []

[extra]
logo = "/img/g1js.svg"
repo = "https://git.duniter.org/libs/g1lib.js/"
website = ""
+++

Bibliothèque <strong>JavaScript</strong> qui permet de manipuler les clés cryptographiques, <a href="https://git.duniter.org/libs/g1lib.js/">ğ1lib</a> est pour l'instant utilisée par Ğsper.