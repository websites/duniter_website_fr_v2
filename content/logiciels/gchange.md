+++
title = "Ğchange"

[taxonomies]
authors = ["kimamila", ]
language = ["javascript", ]
framework = []

[extra]
logo = "/img/gchange.png"
repo = "https://git.duniter.org/marketplaces/gchange-client"
website = "https://www.gchange.fr/"
+++

C'est le logiciel de place de marché le plus utilisé. <a href="https://www.gchange.fr/">Ğchange</a> utilise les Datapod et une interface en <strong>Ionic</strong>.