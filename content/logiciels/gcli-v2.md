+++
title = "Ğcli-v2"

[taxonomies]
authors = []
language = ["rust"]
framework = ["subxt"]

[extra]
logo = "https://duniter.org/img/gcli.svg"
repo = "https://git.duniter.org/clients/rust/gcli-v2s/"
website = ""

+++

<a href="https://git.duniter.org/clients/rust/gcli-v2s/">Ğcli-v2</a> est un client en ligne de commande écrit en <strong>Rust</strong> avec `subxt`.
