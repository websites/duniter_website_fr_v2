+++
title = "Duniter-v2s"

[taxonomies]
authors = ["elois", "HugoTrentesaux", "tuxmain", "bgallois"]
language = ["rust"]
framework = ["substrate"]

[extra]
logo = "/img/duniterv2.svg"
repo = "https://git.duniter.org/nodes/rust/duniter-v2s"
website = "https://duniter.fr/"
+++


<a href="https://git.duniter.org/nodes/rust/duniter-v2s">Duniter-v2s</a> est la version 2 de Duniter totalement ré-écrite sur le framework blockchain Substrate.