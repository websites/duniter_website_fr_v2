+++
title = "ViĞnette"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "/img/vignette.svg"
repo = ""
website = "https://g1cotis.fr/vignette/"

+++

<a href="https://g1cotis.fr/vignette/">ViĞnette</a> est un générateur de QR code permettant de partager facilement sa clé publique.