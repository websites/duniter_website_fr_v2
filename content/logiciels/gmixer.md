+++
title = "Ğmixer"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "/img/gmixer.svg"
repo = ""
website = "https://zettascript.org/projects/gmixer/"

+++

<a href="https://zettascript.org/projects/gmixer/">Ğmixer</a> est un anonymiseur de portefeuille Ğ1 (à ne pas confondre avec le service <a href="https://forum.monnaie-libre.fr/t/gmix-anonymiseur-de-porte-feuille-g1/862">ĞMix</a>).