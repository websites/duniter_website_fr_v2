+++
title = "Ğ1Billet"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "fa-money"
repo = ""
website = "https://g1sms.fr/fr/g1billet"
+++

<a href="https://g1sms.fr/fr/g1billet">Ğ1Billet</a> permet d'imprimer votre propre monnaie, avec des <strong>QR codes</strong> et des billets à gratter.