+++
title = "GecoHelper"

[taxonomies]
authors = ["jytou"]
language = ["java"]
framework = []

[extra]
logo = "/img/gchange.png"
repo = "https://gitlab.com/jytou/geconomicus_helper"
website = ""

+++

Un programme pour aider à animer les jeux Ğeconomicus. [Tutoriel vidéo](https://www.youtube.com/playlist?list=PLTyGJv9Pd00R7LUs8w_CF2EBF9_9qoLLx).