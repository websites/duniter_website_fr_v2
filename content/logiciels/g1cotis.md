+++
title = "Ğ1Cotis"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "fa-cube"
repo = ""
website = "https://g1pourboire.fr/G1cotis.html"
+++

<a href="https://g1pourboire.fr/G1cotis.html">Ğ1Cotis</a> permet de reverser automatiquement un pourcentage des transactions à un compte cible.