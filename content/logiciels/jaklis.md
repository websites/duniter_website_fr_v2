+++
title = "Jaklis"

[taxonomies]
authors = []
language = ["python", ]
framework = []

[extra]
logo = "/img/jaklis.png"
repo = "https://git.p2p.legal/axiom-team/jaklis"
website = ""
+++

<a href="https://git.p2p.legal/axiom-team/jaklis">Jaklis</a> est un client en ligne de commande écrit en <strong>python</strong> pour les datapods Cesium+ et Ğchange.