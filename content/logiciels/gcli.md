+++
title = "Ğcli"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "fa-terminal"
repo = ""
website = ""

+++

<a href="https://git.duniter.org/clients/rust/gcli">Ğcli (v1)</a> est un client GVA en ligne de commande en <strong>Rust</strong>.