+++
title = "Dunixir"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "/img/dunixir.png"
repo = "https://gitlab.imt-atlantique.fr/dunixir/dunixir"
website = ""
+++

<a href="https://gitlab.imt-atlantique.fr/dunixir/dunixir">Dunixir</a> est un projet d'école d'étudiants de l'IMT Atlantique visant à implémenter Duniter en Elixir.