+++
title = "Datapods"

[taxonomies]
authors = ["kimamila"]
language = ["java"]
framework = ["elasticsearch"]

[extra]
logo = "/img/datapod.svg"
repo = "https://git.duniter.org/clients/cesium-grp/cesium-plus-pod/"
website = "http://doc.e-is.pro/cesium-plus-pod/REST_API.html"
+++

Les <a href="http://doc.e-is.pro/cesium-plus-pod/REST_API.html">Datapods</a> sont une couche de données complémentaires hors-blockchain sur <strong>ElasticSearch</strong> qui sert pour les applications clients comme Cesium et Ğchange.