+++
title = "Duniter"

[taxonomies]
authors = ["cgeek", "elois"]
language = ["javascript", "rust"]
framework = ["nodejs"]

[extra]
logo = "/img/duniter-logo_alt.svg"
repo = "https://git.duniter.org/nodes/typescript/duniter/"
website = "https://duniter.fr/"
+++

La blockchain de la Ğ1 fonctionne grâce à <a href="https://git.duniter.org/nodes/typescript/duniter/">Duniter</a>. Historiquement en <strong>Node.js</strong>, une migration progressive vers <strong>Rust</strong> est en cours.