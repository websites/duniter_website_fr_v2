+++
title = "Barre intégrable"

[taxonomies]
authors = ["paidge", ]
language = []
framework = []

[extra]
logo = "fa-battery-half"
repo = ""
website = ""
+++

La <a href="https://git.duniter.org/paidge/barre-de-financement-int-grable">barre de financement intégrable</a> permet d'intégrer dans un site web une barre de progression via une balise <span style="font-family: monospace;">&lt;iframe/&gt;</span> afin de suivre l'évolution d'un financement participatif.