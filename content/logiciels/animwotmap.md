+++
title = "Animwotmap"

[taxonomies]
authors = ["HugoTrentesaux", ]
language = ["julia", ]
framework = []

[extra]
logo = "fa-play-circle"
repo = ""
website = ""
+++

<a href="https://forum.monnaie-libre.fr/t/la-toile-de-confiance-animee/11132">Animwotmap</a> génère une visualisation animée de l'historique de la toile de confiance.