+++
title = "Duniter-squid"

[taxonomies]
authors = []
language = ["typescript"]
framework = []

[extra]
logo = "https://duniter.org/img/duniter-squid.svg"
repo = "https://git.duniter.org/nodes/duniter-squid"
website = ""

+++

<a href="https://git.duniter.org/nodes/duniter-squid">Duniter-squid</a> est un indexeur pour Duniter v2.