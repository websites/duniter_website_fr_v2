+++
title = "Cesium v2"

[taxonomies]
authors = ["kimamila", "txels"]
language = ["typescript",]
framework = ["angularjs", "ionic"]

[extra]
logo = "https://duniter.org/img/cesium-v2.svg"
repo = "https://git.duniter.org/clients/rust/gcli-v2s/"
website = "https://cesium.app/fr/"
+++

<a href="https://cesium.app/">Cesium v2</a> est la transposition de Cesium pour l'écosystème v2.