+++
title = "Ğecko"

[taxonomies]
authors = ["poka", ]
language = ["dart", ]
framework = ["flutter", ]

[extra]
logo = "/img/gecko.png"
repo = "https://git.p2p.legal/axiom-team/gecko"
website = ""
+++

Le framework <strong>Flutter</strong> permet au client mobile axé transaction <a href="https://git.p2p.legal/axiom-team/gecko">Ğecko</a> d'atteindre de très bonnes performances. Les portefeuilles sont gérés via des bindings <strong>Rust</strong> et les données sont échangées via GVA et les Datapods.