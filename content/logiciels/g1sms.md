+++
title = "ĞSMS"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "fa-comment"
repo = ""
website = "https://www.g1sms.fr/"

+++

<a href="https://www.g1sms.fr/">ĞSMS</a> est un système de paiement par SMS qui facilite l'accès à la monnaie aux moins technophiles.