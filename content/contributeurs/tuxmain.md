+++
title = "tuxmain"
description = "développeur de ĞMixer, worldwotmap, backend wotmap, contributeur à Duniter v2"

[extra]
full_name = "Pascal Engélibert"
avatar = "tuxmain.png"
website = "https://txmn.tk/"
forum_duniter = "tuxmain"
forum_ml = "tuxmain"
g1_pubkey = "45GfjkWCWQhJ3epJVGC2NSg1Rcu4Ue1vDD3kk9eLs5TQ"
g1_map = true
gitduniter = "tuxmain"
mastodon = "https://toot.aquilenet.fr/web/@tuxmain"
matrix = "@tuxmain:matrix.txmn.tk"

[taxonomies]
authors = ["tuxmain",]
+++

Étudiant, développeur, libriste.
