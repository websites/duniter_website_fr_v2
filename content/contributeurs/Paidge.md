+++
title = "Paidge"
description = "développeur de la Wotmap et de wotwizard-ui"

[extra]
full_name = "Pierre-Jean Chancellier"
avatar = "Paidge.png"
forum_duniter = "paidge"
forum_ml = "paidge"

[taxonomies]
authors = ["Paidge",]
+++

Paidge est le développeur de la [Wotmap](@/logiciels/wotmap.md) et de [wotwizard-ui](@/logiciels/wotwizard-ui.md)