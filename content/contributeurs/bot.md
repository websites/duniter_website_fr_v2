+++
title = "bot"
description = "robot"

[extra]
full_name = "Bot"
avatar = "TEMPLATE.svg"

[taxonomies]
authors = ["bot",]
+++

Robot utilisé pour une traduction ou une rédaction automatique.