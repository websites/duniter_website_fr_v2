+++
title = "poka"
description = "contributeur actif sur le projet Duniter depuis 2016 aux RML7"

[extra]
full_name = "Étienne Bouché"
avatar = "poka.svg"
forum_duniter = "poka"
forum_ml = "poka"
g1_pubkey = "Do99s6wQR2JLfhirPdpAERSjNbmjjECzGxHNJMiNKT3P"
matrix = "@poka:yuno.librezo.fr"

[taxonomies]
authors = ["poka",]
+++

Je suis contributeur actif sur le projet Duniter depuis 2016 aux RML7 de Laval.

Je code Ğecko en Flutter/Dart.
Je maintiens aussi l’infra Axiom-Team, soit 2 serveurs ProxMox.

J’ai aussi codé Ğ1-stats en bash.
Et jaklis en python.
J’ai aussi codé py-g1-migrator

J’aime la soupe au poireaux.