+++
title = "txels"
description = "contributeur sur plusieurs logiciels"

[extra]
full_name = "txels"
avatar = "txels.jpeg"
forum_duniter = "txels"
gitduniter = "txels"

[taxonomies]
authors = ["txels",]

+++

Txels contribue à plusieurs logiciels comme Duniter v2, Cesium v2, Duniter squid et Ğcli.
