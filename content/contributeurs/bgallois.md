+++
title = "bgallois"
description = "developpeur sur Duniter v2"

[extra]
full_name = "Benjamin Gallois"
avatar = "bgallois.png"
website = "https://gallois.cc/"
forum_duniter = "bgallois"
gitduniter = "bgallois"
+++

Développeur sur Duniter v2 rémunéré par [Axiom-Team](https://axiom-team.fr/) via le financement [ADEME](https://www.ademe.fr/en/).