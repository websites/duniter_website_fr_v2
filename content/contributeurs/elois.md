+++
title = "Elois"
description = "développeur principal de Duniter v2 aujourd'hui en pause"

[extra]
avatar = "elois.jpg"
website = "https://librelois.fr/"
+++

Éloïs est l'initiateur du projet Duniter v2. Aujourd'hui il prend de la distance par rapport au projet.