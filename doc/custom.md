# Page custom

Pour écrire une page custom, veuillez vous inspirer du template `/templates/custom/skeleton.html`.
Créez également une page markdown avec un en-tête spécifiant ce template et les métadonnées habituelles.
Vous pouvez écire une feuille de style en sass dans le dossier `sass` ou en css dans le dossier `static` et l'inclure via le bloc `extrahead`.